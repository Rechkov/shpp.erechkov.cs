package com.shpp.cs.assignments.arrays.tm;

import java.util.ArrayList;

import static java.lang.Math.abs;
import static java.lang.Math.max;

public class ToneMatrixLogic {
    /**
     * Given the contents of the tone matrix, returns a string of notes that should be played
     * to represent that matrix.
     *
     * @param toneMatrix The contents of the tone matrix.
     * @param column     The column number that is currently being played.
     * @param samples    The sound samples associated with each row.
     * @return A sound sample corresponding to all notes currently being played.
     */
    public static double[] matrixToMusic(boolean[][] toneMatrix, int column, double[][] samples) {
        double[] result = new double[ToneMatrixConstants.sampleSize()];
        ArrayList<Integer> lightTurnOnRowIndex = new ArrayList<>();
        double sum;
        double maxSum = 0;
        for (int i = 0; i < toneMatrix.length; i++) {
            if (toneMatrix[i][column] == true) {
                lightTurnOnRowIndex.add(i);
            }
        }
        for (int j = 0; j < samples[0].length; j++) {
            sum = 0;
            for (int i : lightTurnOnRowIndex) {
                sum += samples[i][j];
            }
            result[j] = sum;
            if(maxSum < abs(sum)){
                maxSum = abs(sum);
            }
        }
        if(maxSum != 0){
            for (int i = 0; i <result.length ; i++) {
                result[i] = result[i] / maxSum;
            }
        }else{
            return result;
        }
        return result;
    }
}
