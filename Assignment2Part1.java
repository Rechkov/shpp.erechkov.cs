package com.shpp.erechkov.cs;

import java.util.Scanner;
import com.shpp.cs.a.console.TextProgram;

/**
 * Created by BOSS on 01.05.2016.
 */
public class Assignment2Part1 extends TextProgram {
    public void run() {
        Scanner input  = new Scanner(System.in);
        System.out.print("Please enter a:");
        double a  = input.nextDouble();
        System.out.println();
        System.out.print("Please enter b:");
        double b = input.nextDouble();
        System.out.println();
        System.out.print("Please enter c:");
        double c = input.nextDouble();
        System.out.println();
        double sqrt1 = Math.sqrt(b*b - 4 * a * c);
        double x1  = (- b - sqrt1) / (2 * a);
        double sqrt2 = Math.sqrt(b*b - 4 * a * c);
        double x2  = (- b + sqrt2) / (2 * a);
        if(Double.isNaN(x1)){
            System.out.println("Roots is not real!");
        }else if(x1 == x2) {
            System.out.println("There is one root: " + x2);
        }else{
            System.out.println("Roots are: "+ x1 + " and " + x2);
        }


    }
}
