package com.shpp.erechkov.cs.CSB;

/**
 * Created by BOSS on 31.07.2016.
 */


import java.awt.*;
import javax.swing.JFrame;

public class SerpinskiyTriangle extends JFrame {
    /**
     * To determine height of equilateral triangle you need use this formula (sqrt(3) / 2) * SIDE_LENGTH
     * Coefficient was simplified to double number.
     */
    double HEIGHT_CONVERT_COEFFICIENT = 0.86;//sqrt(3) / 2
    /**
     * This constant responsible for triangle side length relatively to size of the window.
     */
    double TRIANGLE_SIZE_COEFFICIENT = 0.8;//Equals to 80 % of window size
    /**
     * Determine  triangle points deviation to the triangle center, inversely to triangle side length.
     */
    int DEVIATION_CONSTANT = 10;
    /**
     * Number of points for polygon drawing.In our case we have only triangles.
     */
    int NUMBER_OF_POINTS = 3;
    /**
     * X coordinates of triangles dots.
     */
    int[] xCor = new int[NUMBER_OF_POINTS];
    /**
     * Y coordinates of triangles dots.
     */
    int[] yCor = new int[NUMBER_OF_POINTS];
    /**
     * How many recursive calling occur and accordingly number of triangles inside of main triangle.
     */
    int fractalLevel;

    public static void main(String[] args) {
        SerpinskiyTriangle sT = new SerpinskiyTriangle();
    }

    public SerpinskiyTriangle() {
        setSettings(900, 900, 4);
    }

    public void setSettings(int width, int height, int fractalLevel) {
        this.fractalLevel = fractalLevel;
        setSize(width, height);
        setVisible(true);
    }

    public void paint(Graphics g) {
        drawMainTriangle(g);
        drawInnerTriangles(xCor, yCor, fractalLevel, g);
    }

    public void drawMainTriangle(Graphics g) {
        int triangleSideLength = (int) (getWidth() * TRIANGLE_SIZE_COEFFICIENT);
        int deviation = triangleSideLength / DEVIATION_CONSTANT;
        xCor[0] = triangleSideLength / 2 + deviation;
        xCor[1] = deviation;
        xCor[2] = triangleSideLength + deviation;
        yCor[0] = getHeight() - (int) (triangleSideLength * HEIGHT_CONVERT_COEFFICIENT) - deviation;
        yCor[1] = getHeight() - deviation;
        yCor[2] = getHeight() - deviation;
        drawTriangle(xCor, yCor, Color.BLACK, g);
    }

    public void drawInnerTriangles(int[] xCor, int[] yCor, int fractalLevel, Graphics g) {
        int[] midXCor = getAllDotsMid(xCor);
        int[] midYCor = getAllDotsMid(yCor);
        drawTriangle(midXCor, midYCor, Color.WHITE, g);
        if (fractalLevel > 0) {
            drawInnerTriangles(new int[]{xCor[0], midXCor[0], midXCor[2]}, new int[]{yCor[0], midYCor[0], midYCor[2]}, fractalLevel - 1, g);
            drawInnerTriangles(new int[]{xCor[1], midXCor[0], midXCor[1]}, new int[]{yCor[1], midYCor[0], midYCor[1]}, fractalLevel - 1, g);
            drawInnerTriangles(new int[]{xCor[2], midXCor[1], midXCor[2]}, new int[]{yCor[2], midYCor[1], midYCor[2]}, fractalLevel - 1, g);
        }
    }

    public void drawTriangle(int xCor[], int yCor[], Color color, Graphics g) {
        Polygon p = new Polygon(xCor, yCor, NUMBER_OF_POINTS);
        g.setColor(color);
        g.fillPolygon(p);
        g.drawPolygon(p);

    }

    public int getMid(int x1, int x2) {
        return (x1 + x2) / 2;
    }

    public int[] getAllDotsMid(int[] dotsArray) {
        int[] midArray = new int[3];
        midArray[0] = getMid(dotsArray[0], dotsArray[1]);
        midArray[1] = getMid(dotsArray[1], dotsArray[2]);
        midArray[2] = getMid(dotsArray[2], dotsArray[0]);
        return midArray;
    }

}