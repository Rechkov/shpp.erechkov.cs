package com.shpp.erechkov.cs.CSB.StringCalculator;

import com.shpp.cs.a.simple.SimpleProgram;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by BOSS on 27.06.2016.
 */
public class DrawGraphicProgram extends SimpleProgram {
    int TEXTFIELD_WIDTH = 15;
    DrawGraphic drawGraphic = new DrawGraphic();
    JTextField textField = new JTextField(TEXTFIELD_WIDTH);

    public void init() {
        add(new JLabel("y = "), NORTH);
        add(textField, NORTH);
        add(new JLabel("For example:sin(x),pow(x,2) etc"), NORTH);
        addMouseWheelListener(drawGraphic);
        add(drawGraphic);
        textField.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e){
        drawGraphic.formulas = textField.getText();
        drawGraphic.update();
    }

}
