package com.shpp.erechkov.cs.CSB.StringCalculator;

import java.lang.reflect.Method;
import java.util.HashMap;
/**
 * Created by BOSS on 29.07.2016.
 */
public interface StringCalculatorInterface {
    HashMap<String, Double> VARIABLES =  new HashMap<>();
    HashMap<String, Method> MATH_FUNCTIONS =  new HashMap<>();
}
