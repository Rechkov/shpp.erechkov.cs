package com.shpp.erechkov.cs.CSB.StringCalculator;

import com.shpp.erechkov.cs.CSB.Collections.MyArrayList;
import com.shpp.erechkov.cs.CSB.Collections.MyHashMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;


/**
 * Created by BOSS on 15.06.2016.
 */
public class StringCalculator implements StringCalculatorInterface {

    public static void main(String[] args) {
        //pow(sqrt(exp(max(2,1))),sqrt(cos(30))) - ACCEPTABLE
        run();
    }

    public static void run() {
        getMathFunctions();
        putVariables();
        while (true) {
            try {
                String formula = inputFormula();
                if (!isCorrectBracketsSequence(formula)) {
                    continue;
                }
                String result = calculateFormula(formula);
                if (result.equals("")) {
                    System.out.println("ERROR");
                    continue;
                }
                System.out.println(result);
            } catch (Exception e) {
                System.out.println("ERROR");
            }
        }
    }

    public static void putVariables() {
        VARIABLES.put("x", 2.0);
        VARIABLES.put("b", 2.0);
        VARIABLES.put("c", 15.0);
    }

    public static String calculateFormula(String formula) {
        formula = formula.replace(" ", "");//Transform to normal view.
        formula = substituteVariables(formula, VARIABLES);
        formula = calculateAllMathFunctions(formula);
        formula = calculateAllBracketsExpressions(formula);
        formula = executeRawCalculation(formula);
        return formula;
    }

    public static void getMathFunctions() {
        for (Method m : Math.class.getMethods()) {
            MATH_FUNCTIONS.put(m.getName(), m);
        }
    }

    public static String substituteVariables(String line, MyHashMap<String, Double> variables) {
        char[] lineChars = line.toCharArray();
        for (int i = 0; i < lineChars.length; i++) {
            lineChars = line.toCharArray();
            String element = String.valueOf(lineChars[i]);
            if (i + 1 < lineChars.length) {
                if (!Character.isLetter(lineChars[i + 1]) && variables.containsKey(element) && lineChars[i + 1] != '(') { // replace only single variables
                    String replacedValue = String.valueOf(variables.get(element));
                    line = line.substring(0, i) + replacedValue + line.substring(i + 1);
                }
            } else if (variables.containsKey(element)) {//in case variables the last element in string line
                String replacedValue = String.valueOf(variables.get(element));//
                line = line.substring(0, i) + replacedValue + line.substring(i + 1);
            }
        }
        return line;
    }

    public static String calculateAllBracketsExpressions(String line) {
        if (line.equals(""))
            return "";
        while (isBracketInLine(line)) {
            line = calculateBracketExpression(line);
        }
        return line;
    }

    public static String calculateBracketExpression(String line) {
        int startBracketIndex = 0;
        int endBracketIndex;
        boolean noBracketsInLine = true;
        char[] lineChars = line.toCharArray();
        for (int i = 0; i < lineChars.length; i++) {
            if (lineChars[i] == '(') {
                lineChars = line.toCharArray();
                noBracketsInLine = false;
                startBracketIndex = i;
            }
            if (lineChars[i] == ')' && !noBracketsInLine) {
                endBracketIndex = i;
                String replacedLine = line.substring(startBracketIndex + 1, endBracketIndex);
                line = line.replace("(" + replacedLine + ")", executeRawCalculation(replacedLine));
                noBracketsInLine = true;
            }
        }
        return line;
    }

    public static boolean isBracketInLine(String line) {
        char[] lineChars = line.toCharArray();
        for (int i = 0; i < lineChars.length; i++) {
            if (lineChars[i] == ')' || lineChars[i] == '(') {
                return true;
            }
        }
        return false;
    }

    public static boolean isCorrectBracketsSequence(String line) {
        int countBrackets = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '(')
                countBrackets++;
            if (line.charAt(i) == ')')
                countBrackets--;
        }
        if (countBrackets > 0) {
            System.out.println("Not enough ')'");
            return false;
        }
        if (countBrackets < 0) {
            System.out.println("Not enough '('");
            return false;
        }
        return true;
    }

    public static MyArrayList<String> parseValuesAndOperators(String line) {
        MyArrayList<String> valuesAndOperators = new MyArrayList<>();//List that contain divided number and signs
        if (line.equals("")) {
            valuesAndOperators.add("");
            return valuesAndOperators;
        }
        String oneElement = "";
        boolean numberHasMinus = false;
        char[] lineChars = line.toCharArray();
        if (lineChars[0] == '-') {
            numberHasMinus = true;
        }
        for (int i = 0; i < lineChars.length; i++) {
            if (Character.isDigit(lineChars[i]) || lineChars[i] == '.') {//Check if current char is digit
                oneElement += lineChars[i];
                if (i + 1 >= lineChars.length) {//Check if it was last char in line
                    valuesAndOperators.add(oneElement);
                }
            } else { //Go to else if meets sign
                if (numberHasMinus) {
                    oneElement = "-" + oneElement;//Add - to the number
                    numberHasMinus = false;
                } else {
                    valuesAndOperators.add(oneElement);//Add detached number
                    oneElement = String.valueOf(lineChars[i]);
                    valuesAndOperators.add(oneElement);//Add  sign
                    oneElement = "";
                }
                if (i + 1 < lineChars.length && lineChars[i + 1] == '-')
                    numberHasMinus = true;//Check if the next number has '-'
            }
        }
        convertTwoMinusToPlus(valuesAndOperators);//To avoid situation when numbersAndSigns contains number: --2,--10.44378 etc
        return valuesAndOperators;
    }

    public static void convertTwoMinusToPlus(MyArrayList<String> numbersAndSigns) {
        for (int i = 0; i < numbersAndSigns.size(); i++) {
            numbersAndSigns.set(i, numbersAndSigns.get(i).replace("--", ""));
        }
    }

    /**
     * @param parameters array of parameters of current formula useful when you want to call formulas
     *                   with different amount of parameters.
     * @return String that contain calculated value.
     */
    public static String findMathFormulaAndCalculate(Method m, MyArrayList<String> parameters) {
        Method method;
        String result;
        double arg1;
        double arg2 = 0;
        if (parameters.size() == 3) {
            arg1 = Double.parseDouble(parameters.get(0));
            arg2 = Double.parseDouble(parameters.get(2));//2 index because at index 1 will be coma ","
        } else if (parameters.size() == 1) {
            arg1 = Double.parseDouble(parameters.get(0));
        } else {
            return "";
        }
        try {
            if (m.getParameters().length == 1) {
                Class[] paramTypes = m.getParameterTypes();
                method = Math.class.getMethod(m.getName(), paramTypes);
                if (Arrays.toString(m.getParameters()).charAt(1) == 'i') {//i stand for int ,to check while Functions have int parameters
                    int iResult = (int) method.invoke(Math.class, (int) arg1);//For example Math.floorDiv(int x ,int y)
                    result = String.valueOf(iResult);
                } else {
                    double dResult = (double) method.invoke(Math.class, arg1);//call the method and convert result to double
                    if (Double.isNaN(dResult))
                        return "";
                    result = BigDecimal.valueOf(dResult).toPlainString();//BigDecimal is required to avoid 1.19479E-10 results
                }                                                       //that can lead to wrong calculation
                return result;

            }
            if (m.getParameters().length == 2) {
                Class[] paramTypes = m.getParameterTypes();
                method = Math.class.getMethod(m.getName(), paramTypes);
                if (Arrays.toString(m.getParameters()).charAt(1) == 'i') {
                    int iResult = (int) method.invoke(Math.class, (int) arg1, (int) arg2);
                    result = String.valueOf(iResult);
                } else {
                    double dResult = (double) method.invoke(Math.class, arg1, arg2);
                    if (Double.isNaN(dResult))
                        return "";
                    result = BigDecimal.valueOf(dResult).toPlainString();
                }
                return result;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String executeRawCalculation(String line) {
        if (line.equals(""))
            return "";
        MyArrayList<String> numbersAndSigns = parseValuesAndOperators(line);
        if (numbersAndSigns.size() > 1) {
            calculateSimpleExpression(numbersAndSigns, "/");
            calculateSimpleExpression(numbersAndSigns, "*");
            calculateSimpleExpression(numbersAndSigns, "-");
            calculateSimpleExpression(numbersAndSigns, "+");
        }
        return numbersAndSigns.get(0);
    }

    public static void calculateSimpleExpression(MyArrayList<String> numbersAndSigns, String sign) {
        double result = 0;
        double firstNumber;
        double secondNumber;
        for (int i = 0; i < numbersAndSigns.size(); i++) {
            if (numbersAndSigns.get(i).equals(sign)) {
                firstNumber = Double.parseDouble(numbersAndSigns.get(i - 1));
                secondNumber = Double.parseDouble(numbersAndSigns.get(i + 1));
                numbersAndSigns.remove(i - 1);
                numbersAndSigns.remove(i);//In fact we remove i+1 but due to previous operation all elements shifted to the left and i decreases
                if (sign.equals("+")) result = firstNumber + secondNumber;
                if (sign.equals("-")) result = firstNumber - secondNumber;
                if (sign.equals("*")) result = firstNumber * secondNumber;
                if (sign.equals("/")) result = firstNumber / secondNumber;
                numbersAndSigns.set(i - 1, BigDecimal.valueOf(result).toPlainString());
                i--;
            }
        }
    }

    public static String calculateAllMathFunctions(String line) {
        if (line.equals(""))
            return "";
        while (isFunctionsInFormula(line)) {
            line = calculateMathFunctions(line);
        }
        return line;
    }

    public static String calculateMathFunctions(String line) {
        int startIndex;
        String mathNameFormula = "";
        char[] charLine = line.toCharArray();
        for (int i = 0; i < charLine.length; i++) {
            if (Character.isLetter(charLine[i])) {
                mathNameFormula += charLine[i];//If few letters in a row, concatenate it on one string.
            } else {
                if (charLine[i] == '(' && mathNameFormula.length() > 2) {//Otherwise, if next symbol of letters row is the '(' - it's the formula!
                    startIndex = i + 1;//initial index from which method's parameters start
                    if (!MATH_FUNCTIONS.containsKey(mathNameFormula)) {
                        System.out.println("No such formula in Math Class");
                        return "";
                    }
                    line = calculateParameters(line, mathNameFormula, startIndex);
                    return line;
                }
                charLine = line.toCharArray();//renew charLine dew to length has been changed
            }
        }
        return line;
    }

    public static int findExitBracketOfParameter(int startBracketIndex, char[] charLine) {
        int innerBracketsCount = 0;
        for (int j = startBracketIndex + 1; j < charLine.length; j++) {
            if (j < charLine.length) {
                if (charLine[j] == '(')//Check for inner brackets,increase innerBracketsCount to avoid inner exit brackets
                    innerBracketsCount++;
                if (charLine[j] == ')') {
                    if (innerBracketsCount == 0) {
                        return j;
                    }
                    innerBracketsCount--;
                }
            }
        }
        return 0;
    }

    public static String calculateParameters(String line, String mathNameFormula, int startIndex) {
        String parameters;
        char[] charLine = line.toCharArray();
        int endIndex = findExitBracketOfParameter(startIndex, charLine);
        parameters = line.substring(startIndex, endIndex);//include all character between '(' and ')' of the function
        String previousParameters = parameters;//This will be replaced by the parameter we will find out
        parameters = calculateParametersSeparately(parameters);//calculateParameters() Use calculateString() method that creates recursion
        line = line.replace(previousParameters, parameters);
        String mathResult = findMathFormulaAndCalculate(MATH_FUNCTIONS.get(mathNameFormula), parseValuesAndOperators(parameters));//calculate one single number-result
        if (mathResult == null) {
            System.out.println("Formula Error");
            return "";
        }
        line = line.replace(mathNameFormula + "(" + parameters + ")", mathResult);
        return line;
    }

    public static String calculateParametersSeparately(String parameters) {
        int comaIndex = checkForComas(parameters);
        if (comaIndex == -1) {//If parameters line don't have coma ,it means it have only one argument
            if (isNumber(parameters))
                return parameters;
            return calculateFormula(parameters);
        } else {//For 2 arguments in parameters line
            String parameters1 = parameters.substring(0, comaIndex);
            String parameters2 = parameters.substring(comaIndex + 1, parameters.length());
            if (!isNumber(parameters1))
                parameters1 = calculateFormula(parameters1);
            if (!isNumber(parameters2))
                parameters2 = calculateFormula(parameters2);
            return parameters1 + "," + parameters2;
        }
    }

    public static boolean isNumber(String line) {
        for (int i = 1; i < line.length(); i++) {
            if ((Character.isDigit(line.charAt(i)) || line.charAt(i) == '.')) {
                continue;
            }
            return false;
        }
        return true;
    }

    public static int checkForComas(String line) {
        int countBrackets = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '(')
                countBrackets++;
            if (line.charAt(i) == ')')
                countBrackets--;
            if (line.charAt(i) == ',' && countBrackets == 0)
                return i;
        }
        return -1;
    }

    public static boolean isFunctionsInFormula(String line) {
        char[] charLine = line.toCharArray();
        String mathNameFormula = "";
        for (int i = 0; i < charLine.length; i++) {
            if (Character.isLetter(charLine[i])) {
                mathNameFormula += charLine[i];//If few letters in a row, concatenate it on one string.
            } else {
                if (charLine[i] == '(' && mathNameFormula.length() > 2)//Otherwise, if next symbol of letters row is the '(' - it's the formula!
                    return true;
            }
        }
        return false;
    }

    public static String inputFormula() {
        System.out.print("Enter formula: ");
        try {
            BufferedReader lineFormula = new BufferedReader(new InputStreamReader(System.in));
            return lineFormula.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
