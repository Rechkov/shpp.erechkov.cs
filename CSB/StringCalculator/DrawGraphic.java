package com.shpp.erechkov.cs.CSB.StringCalculator;

import acm.graphics.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.Method;
import java.math.BigDecimal;


/**
 * Created by BOSS on 26.06.2016.
 */
public class DrawGraphic extends GCanvas implements ComponentListener, MouseWheelListener, StringCalculatorInterface {
    double SEP_WIDTH = 10;
    int PRECISION_COEFFICIENT = 4;
    double LIMITER_OF_MINIMUM_SCALE = 0.0001;
    double SEPARATION = 50;
    double LimitOfStartCountInDecimal = 0.9;
    double numberInterval = 1;
    double DecimalMinScaleInterval = 0.1;
    static String formulas = "";

    public DrawGraphic() {
        addComponentListener(this);
        addMouseWheelListener(this);
    }


    public void drawGraphic() {
        Color BACKGROUND_COLOR = new Color(47, 57, 69);
        setBackground(BACKGROUND_COLOR);
        drawAxisSeparators(SEPARATION);
        if (!formulas.equals(""))
            drawFunction(formulas);
        drawGrid();
    }

    public void update() {
        removeAll();
        drawGraphic();
    }


    public void drawGrid() {
        drawMainAxis();
        drawPointers();
    }

    public void drawMainAxis() {
        //X axis
        GLine gridLineX = new GLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
        gridLineX.setColor(Color.white);
        add(gridLineX);
        //Y axis
        GLine gridLineY = new GLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
        gridLineY.setColor(Color.white);
        add(gridLineY);
    }

    public void drawPointers() {
        GLine leftLinePointerY = new GLine(getWidth() / 2, 0, getWidth() / 2 + SEP_WIDTH / 2, 0 + SEP_WIDTH);
        leftLinePointerY.setColor(Color.white);
        add(leftLinePointerY);
        GLine rightLinePointerY = new GLine(getWidth() / 2, 0, getWidth() / 2 - SEP_WIDTH / 2, 0 + SEP_WIDTH);
        rightLinePointerY.setColor(Color.white);
        add(rightLinePointerY);
        GLine leftLinePointerX = new GLine(getWidth(), getHeight() / 2, getWidth() - SEP_WIDTH, getHeight() / 2 - SEP_WIDTH / 2);
        leftLinePointerX.setColor(Color.white);
        add(leftLinePointerX);
        GLine rightLinePointerX = new GLine(getWidth(), getHeight() / 2, getWidth() - SEP_WIDTH, getHeight() / 2 + SEP_WIDTH / 2);
        rightLinePointerX.setColor(Color.white);
        add(rightLinePointerX);
    }

    public void drawAxisSeparators(Double separation) {
        double numberOfXSeparators = getWidth() / separation;
        double numberOfYSeparators = getHeight() / separation;
        for (double i = 0; i < numberOfXSeparators / 2; i++) {
            double distance = separation * i;//from 0 coordinate
            drawAxisXSeparator(distance);
            drawAxisXLabels(distance, numberInterval, i);
            if (numberOfYSeparators / 2.0 > i) {
                drawAxisYSeparator(distance);
                drawAxisYLabels(distance, numberInterval, i);
            }
        }
    }

    public void drawAxisXSeparator(Double coordinateX) {
        double centerX = getWidth() / 2;
        GLine separatorLeftOfY = new GLine(centerX - coordinateX, getHeight() / 2 - SEP_WIDTH / 2, centerX - coordinateX, getHeight() / 2 + SEP_WIDTH / 2);
        separatorLeftOfY.setColor(Color.white);
        add(separatorLeftOfY);
        GLine separatorRightOfY = new GLine(centerX + coordinateX, getHeight() / 2 - SEP_WIDTH / 2, centerX + coordinateX, getHeight() / 2 + SEP_WIDTH / 2);
        separatorRightOfY.setColor(Color.white);
        add(separatorRightOfY);
    }

    public void drawAxisYSeparator(Double coordinateY) {
        double centerY = getHeight() / 2;
        GLine separatorBelowX = new GLine(getWidth() / 2 - SEP_WIDTH / 2, centerY + coordinateY, getWidth() / 2 + SEP_WIDTH / 2, centerY + coordinateY);
        separatorBelowX.setColor(Color.white);
        add(separatorBelowX);
        GLine separatorUnderX = new GLine(getWidth() / 2 - SEP_WIDTH / 2, centerY - coordinateY, getWidth() / 2 + SEP_WIDTH / 2, centerY - coordinateY);
        separatorUnderX.setColor(Color.white);
        add(separatorUnderX);

    }

    public void drawAxisXLabels(double coordinateX, double numberInterval, double i) {
        double centerX = getWidth() / 2;
        double coordinateY = getHeight() / 2 + SEP_WIDTH / 2;
        if (i == 0)
            return;
        String nameLabel = String.valueOf(preciseMultiply(i, numberInterval));
        GLabel labelLeftOfY = new GLabel("-" + nameLabel);
        labelLeftOfY.setLocation(centerX - coordinateX - labelLeftOfY.getWidth() / 2, coordinateY + labelLeftOfY.getAscent());
        labelLeftOfY.setColor(Color.white);
        add(labelLeftOfY);
        GLabel labelRightOfY = new GLabel(nameLabel);
        labelRightOfY.setLocation(centerX + coordinateX - labelLeftOfY.getWidth() / 2, coordinateY + labelLeftOfY.getAscent());
        labelRightOfY.setColor(Color.white);
        add(labelRightOfY);
    }

    public void drawAxisYLabels(double coordinateY, double numberInterval, double i) {
        double centerY = getHeight() / 2;
        double centerX = getWidth() / 2;
        String nameLabel = String.valueOf(preciseMultiply(i, numberInterval));
        GLabel labelAboveOfX = new GLabel(nameLabel);
        labelAboveOfX.setLocation(centerX + SEP_WIDTH, centerY - coordinateY - labelAboveOfX.getDescent());
        labelAboveOfX.setColor(Color.white);
        add(labelAboveOfX);
        if (nameLabel.equals("0.0"))
            return;
        GLabel labelBelowOfX = new GLabel("-" + nameLabel);
        labelBelowOfX.setLocation(centerX + SEP_WIDTH, centerY + coordinateY - labelAboveOfX.getDescent());
        labelBelowOfX.setColor(Color.white);
        add(labelBelowOfX);
    }

    public void drawFunction(String formula) {
        getMathFunctions();
        double d = (getWidth() / SEPARATION) / 2.0 * numberInterval;//(range of definition X ) / 2
        double yRange = (getWidth() / SEPARATION) / 2.0 * numberInterval;//(range of definition Y ) / 2
        double intervalPrecision = d / (SEPARATION * PRECISION_COEFFICIENT);//How precise function with each x will be calculated
        double xStartCor = -d; //Start line  x coordinate
        double yStartCor = replaceXbyValueAndCalculate(formula, -d);//Start y
        double xEndCor;//End line x coordinate
        double yEndCor;//End y
        double centerX = getWidth() / 2;
        double centerY = getHeight() / 2;
        for (double x = -d; x <= d; x += intervalPrecision) {
            double y = replaceXbyValueAndCalculate(formula, x);//Actual math y value of appropriate x;
            if (Double.isNaN(y)) {
                continue;
            }
            if (Math.abs(y) > yRange) {
                if (y > 0)
                    yEndCor = 0;
                else
                    yEndCor = getHeight();
            } else {
                yEndCor = centerY - preciseMultiply(SEPARATION, y / numberInterval);
            }
            xEndCor = centerX + preciseMultiply(SEPARATION, x / numberInterval);
            drawLinesFunction(xStartCor, yStartCor, xEndCor, yEndCor);
            xStartCor = xEndCor;
            yStartCor = yEndCor;

        }

    }

    public Double replaceXbyValueAndCalculate(String mathFormula, double value) {
        mathFormula = mathFormula.replace("xv", BigDecimal.valueOf(value).toPlainString());
        String result = StringCalculator.calculateFormula(mathFormula);
        if (result.equals("") || result.equals("ERROR")) {
            return Double.NaN;
        }
        return Double.parseDouble(result);

    }

    public static void getMathFunctions() {
        for (Method m : Math.class.getMethods()) {
            MATH_FUNCTIONS.put(m.getName(), m);
        }
    }

    public void drawLinesFunction(double x0, double y0, double x1, double y1) {
        Color GRAPHIC_COLOR = new Color(66, 139, 202);
        GLine dot = new GLine(x0 - 1, y0, x1 - 1, y1);
        dot.setColor(GRAPHIC_COLOR);
        add(dot);
    }

    public double preciseMultiply(double a, double b) {
        return BigDecimal.valueOf(a).multiply(BigDecimal.valueOf(b)).doubleValue();

    }

    public double preciseAdd(double a, double b) {
        return BigDecimal.valueOf(a).add(BigDecimal.valueOf(b)).doubleValue();
    }

    public double preciseSubtract(double a, double b) {
        return BigDecimal.valueOf(a).subtract(BigDecimal.valueOf(b)).doubleValue();
    }

    public double preciseDevision(double a, double b) {
        return BigDecimal.valueOf(a).divide(BigDecimal.valueOf(b)).doubleValue();
    }

    public void componentHidden(ComponentEvent e) {
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentResized(ComponentEvent e) {

        update();
    }

    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        double tempNumberInterval;
        int notches = e.getWheelRotation();
        if (notches > 0) {
            if (numberInterval <= LimitOfStartCountInDecimal) {
                tempNumberInterval = preciseAdd(numberInterval, DecimalMinScaleInterval);
                if (tempNumberInterval == DecimalMinScaleInterval * SEP_WIDTH) {
                    numberInterval = preciseAdd(numberInterval, DecimalMinScaleInterval);
                    DecimalMinScaleInterval = DecimalMinScaleInterval * SEP_WIDTH;
                } else
                    numberInterval = tempNumberInterval;
            } else
                numberInterval = preciseAdd(numberInterval, 1);
            update();
        } else {
            if (numberInterval - 1 <= 1) {
                tempNumberInterval = preciseSubtract(numberInterval, DecimalMinScaleInterval);
                if (tempNumberInterval == 0) {
                    DecimalMinScaleInterval = DecimalMinScaleInterval /SEP_WIDTH;
                    if (DecimalMinScaleInterval == LIMITER_OF_MINIMUM_SCALE) {
                        DecimalMinScaleInterval = LIMITER_OF_MINIMUM_SCALE * SEP_WIDTH;
                        return;
                    }
                    numberInterval = preciseSubtract(numberInterval, DecimalMinScaleInterval);
                } else
                    numberInterval = tempNumberInterval;
            } else
                numberInterval = preciseSubtract(numberInterval, 1);
            update();
        }
    }


}
