package com.shpp.erechkov.cs.CSB;

import com.shpp.erechkov.cs.CSB.Collections.Element;
import com.shpp.erechkov.cs.CSB.Collections.MyHashMap;
import com.shpp.erechkov.cs.CSB.Collections.MyPriorityQueue;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by BOSS on 14.07.2016.
 */
public class HuffmanCompressor {

    public static int fileBytesLength;
    public static Node mainNode;

    static class Node implements Comparable<Node>, Serializable {
        Node right;
        Node left;
        byte name;
        Integer freq;
        boolean isSimple = false;//Simple mean it do not contains further nodes
        BitSet encodedBitRow;
        int fileByteLength;

        @Override
        public int compareTo(Node other) {
            if (freq > other.freq)
                return 1;
            if (freq.equals(other.freq))
                return 0;
            else
                return -1;
        }
    }

    public static void main(String[] args) {
        double start = System.currentTimeMillis();
        compressFile("files/test.bmp", "files/testCompressed.txt");
        decompressFile("files/testCompressed.txt", "files/testDecompressed.bmp");
        double end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    private static MyHashMap<Byte, String> buildEncodingTree(byte[] fileBytes) {
        MyHashMap<Byte, Integer> freqTree = getFrequencyTree(fileBytes);
        MyPriorityQueue<Node> nodesQu = createNodesQueue(freqTree);
        mainNode = buildMainNode(nodesQu);
        MyHashMap<Byte, String> encodingTree = new MyHashMap<>();
        putEncodingTreeElements(mainNode, "", encodingTree);
        mainNode.fileByteLength = fileBytesLength;//Save file byte length in the node variable
        return encodingTree;
    }

    private static MyPriorityQueue<Node> createNodesQueue(MyHashMap<Byte, Integer> freqTree) {
        MyPriorityQueue<Node> nodesQu = new MyPriorityQueue<>();
        for (Element<Byte,Integer> b : freqTree) {
            Node node = new Node();
            node.freq = b.getValue();
            node.name = b.getKey();
            node.isSimple = true;
            nodesQu.add(node);
        }
        return nodesQu;
    }

    private static Node buildMainNode(MyPriorityQueue<Node> nodesQu) {
        Node mainNode = null;
        while (!nodesQu.isEmpty()) {
            if (nodesQu.size() == 1) {
                mainNode = nodesQu.poll();
                break;
            }
            Node concatenateNode = new Node();
            concatenateNode.left = nodesQu.poll();
            concatenateNode.right = nodesQu.poll();
            concatenateNode.freq = concatenateNode.left.freq + concatenateNode.right.freq;
            nodesQu.add(concatenateNode);
        }
        return mainNode;
    }


    private static void putEncodingTreeElements(Node node, String code, MyHashMap<Byte, String> encodingTree) {
        if (node.isSimple) {
            encodingTree.put(node.name, code);
            return;
        }
        putEncodingTreeElements(node.right, code + "1", encodingTree);
        putEncodingTreeElements(node.left, code + "0", encodingTree);
    }

    private static MyHashMap<Byte, Integer> getFrequencyTree(byte[] fileBytes) {
        MyHashMap<Byte, Integer> freqMap = new MyHashMap<>();
        for (byte b : fileBytes) {
            if (freqMap.containsKey(b)) {
                freqMap.put(b, freqMap.get(b) + 1);
            } else {
                freqMap.put(b, 1);
            }
        }
        fileBytesLength = fileBytes.length;//Save file byte length in scope variable
        return freqMap;
    }

    static BitSet encodeFile(String filePath) {
        byte[] fileBytes = openFile(filePath);
        MyHashMap<Byte, String> encodingTree = buildEncodingTree(fileBytes);
        return createEncodedBitRow(fileBytes, encodingTree);
    }

    private static BitSet createEncodedBitRow(byte[] fileBytes, MyHashMap<Byte, String> encodingTree) {
        BitSet bs = new BitSet();
        int bitIndex = 0;
        String s;
        for (byte b : fileBytes) {
            s = encodingTree.get(b);
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '1') {
                    bs.set(bitIndex);
                }
                bitIndex++;
            }
        }
        bs.set(bitIndex);
        return bs;
    }


    static Node openCompressedFile(String filePath) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath));
            return (Node) objectInputStream.readObject();//Return main Node
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }

    static byte[] decodeFile(String filePath) {
        Node mainNode = openCompressedFile(filePath);
        Node tempNode = mainNode;
        byte[] decodedArray = new byte[fileBytesLength];
        int count = 0;
        for (int i = 0; i < mainNode.encodedBitRow.length(); i++) {
            if (mainNode.encodedBitRow.get(i)) {
                if (tempNode.right.isSimple) {
                    decodedArray[count] = tempNode.right.name;
                    count++;
                    tempNode = mainNode;
                } else {
                    tempNode = tempNode.right;
                }
            } else {
                if (tempNode.left.isSimple) {
                    decodedArray[count] = tempNode.left.name;
                    count++;
                    tempNode = mainNode;
                } else {
                    tempNode = tempNode.left;
                }
            }
        }
        return decodedArray;
    }

    static void compressFile(String openFilePath, String saveFilePath) {
        try{
            BitSet bs = encodeFile(openFilePath);
            mainNode.encodedBitRow = bs;
            saveCompressedFile(saveFilePath, mainNode);
        }
        catch (NullPointerException e){
            System.out.println("File is empty");
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

    static void decompressFile(String openFilePath, String saveFilePath) {
        try{
            saveDecompressedFile(saveFilePath, decodeFile(openFilePath));
        }catch(Exception e){

        }
    }

    public static byte[] openFile(String filePath) {
        try {
            Path path = Paths.get(filePath);
            return Files.readAllBytes(path);
        }catch (NoSuchFileException e){
            System.out.println("No such file");
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveCompressedFile(String filePath, Node mainNode) {
        try {
            FileOutputStream file = new FileOutputStream(filePath);
            ObjectOutputStream oos = new ObjectOutputStream(file);
            oos.writeObject(mainNode);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveDecompressedFile(String saveFilePath, byte[] byteArray) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(saveFilePath);
            fileOutputStream.write(byteArray);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
