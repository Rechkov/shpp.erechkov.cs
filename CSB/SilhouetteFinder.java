package com.shpp.erechkov.cs.CSB;

import com.shpp.erechkov.cs.CSB.Collections.MyArrayList;
import com.shpp.erechkov.cs.CSB.Collections.MyLinkedList;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by BOSS on 29.06.2016.
 */
public class SilhouetteFinder {
    final private static double REMOVE_BORDER_LEVEL_CONSTANT = 10000.0;//Were discovered in practical way
    final private static int NEIGHBOURS_TO_CHECK = 6;
    private static int COLOR_DEVIATION = 230;//This parameter control which pixel will be assumed as "black"
    private static int NUMBER_OF_PROCESSORS = 3;
    private static int SIZE_OF_SMALL_OBJECT_CONSTANT = 5;//This var in denominator.For example "5" its equivalent of 20% fo average object size.
    private static int silhouetteCount = 0;
    private static int totalAmountOfBlackPixels = 0;
    private static MyArrayList<Dot> listOfSilhouettesOutlineDots = new MyArrayList<>();
    private static int objectPixelsSize;
    private static boolean firstTimeSilhouetteCount = true;

    static class Dot implements Comparable {
        int x;
        int y;

        public Dot(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Object o) {
            return 0;
        }
    }

    public static void main(String[] args) {
        double start = System.currentTimeMillis();
        System.out.println(countSilhouettes("files/HiRes.jpg"));
        double end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public static int countSilhouettes(String imagePath) {
        boolean[][] image = openImage(imagePath);
        image = rawCountSilhouette(image);//calculate count, totalAmountOfBlackPixels, listOfSizesAndPoints and listOfBorderDots
        firstTimeSilhouetteCount = false;
        image = removeBorder(image, calculateRemoveLevel());
        image = removeSmallObjects(image);
        rawCountSilhouette(image);
        return silhouetteCount;
    }

    public static int calculateRemoveLevel() {
        if (silhouetteCount == 0)//If no Silhouettes in picture ,there is nothing to remove.
            return 0;
        int average = totalAmountOfBlackPixels / silhouetteCount;
        if (average / REMOVE_BORDER_LEVEL_CONSTANT < 0.1)
            return 0;
        if (average / REMOVE_BORDER_LEVEL_CONSTANT < 1)
            return 1;
        return average / (int) REMOVE_BORDER_LEVEL_CONSTANT;
    }

    /**
     * Despite the fact this function is void it calculate variables defined in scope
     * such as: objectPixelSize - the amount of pixel of current object
     * silhouetteCount ,totalAmountOfBlackPixels,listOfBorderDots.
     */
    public static void BFSCount(boolean[][] image, int x, int y, boolean[][] visited) {

        int width = image.length;
        int height = image[0].length;
        Dot dot = new Dot(x, y);
        if (visited[x][y]) {
            return;
        }
        visited[x][y] = true;
        MyLinkedList<Dot> queue = new MyLinkedList<>();
        queue.offer(dot);
        objectPixelsSize = 1;
        while (!queue.isEmpty()) {
            Dot pulledDot = queue.poll();
            int startX = pulledDot.x;
            int startY = pulledDot.y;
            int neighboursNumber = 0;
            for (int i = -1; i <= 1; i++) {//Check if any of 8 neighbours dots is black if yes add to the queue
                for (int j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0)
                        continue;
                    int corX = startX + i;
                    int corY = startY + j;
                    if (corX < width && corX >= 0 && corY < height && corY >= 0) {
                        if (!visited[corX][corY] && image[corX][corY]) {
                            queue.offer(new Dot(corX, corY));
                            objectPixelsSize += 1;
                            visited[corX][corY] = true;
                        }
                        if (image[corX][corY])
                            neighboursNumber++;
                    }
                }
            }
            if (neighboursNumber <= NEIGHBOURS_TO_CHECK && firstTimeSilhouetteCount)
                listOfSilhouettesOutlineDots.add(new Dot(startX, startY));//Add border dots to avoid finding it again  further
        }
        if (firstTimeSilhouetteCount) {
            totalAmountOfBlackPixels += objectPixelsSize;
        }
        silhouetteCount++;
    }

    public static boolean[][] rawCountSilhouette(boolean[][] image) {
        silhouetteCount = 0;
        if (image == null)
            return null;
        boolean[][] visited = new boolean[image.length][image[0].length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                if (image[i][j] && !visited[i][j]) {
                    BFSCount(image, i, j, visited);
                }
            }
        }
        return image;
    }

    public static boolean[][] openImage(String imagePath) {
        try {
            BufferedImage image = ImageIO.read(new File(imagePath));
            return openImageInThreads(image);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean[][] openImageInThreads(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        boolean[][] imageArray = new boolean[width][height];
        int separationInterval = width / NUMBER_OF_PROCESSORS;
        MyArrayList<Thread> threadList = new MyArrayList<>();
        Thread t;
        for (int i = 0; i < NUMBER_OF_PROCESSORS; i++) {
            t = openOneThread(i, separationInterval, imageArray, image);
            t.start();
            threadList.add(t);
        }
        try {
            for (Thread thread : threadList) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return imageArray;
    }

    public static Thread openOneThread(int imagePartToProcess, int separationInterval, boolean[][] imageArray, BufferedImage image) {
        Thread t = new Thread(() -> {
            int reminder = 0;
            if (imagePartToProcess + 1 >= NUMBER_OF_PROCESSORS) {
                reminder = image.getWidth() % NUMBER_OF_PROCESSORS;
            }
            for (int i = imagePartToProcess * separationInterval; i < (imagePartToProcess + 1) * separationInterval + reminder; i++) {
                for (int j = 0; j < imageArray[0].length; j++) {
                    if (isBlack(image, i, j)) {
                        imageArray[i][j] = true;
                    }
                }
            }
        });
        return t;
    }

    public static boolean isBlack(BufferedImage image, int x, int y) {
        Color c = new Color(image.getRGB(x, y), true);
        return c.getRed() < COLOR_DEVIATION && c.getBlue() < COLOR_DEVIATION && c.getGreen() < COLOR_DEVIATION;
    }

    public static boolean[][] removeSmallObjects(boolean[][] image) {
        boolean[][] visited = new boolean[image.length][image[0].length];
        if (silhouetteCount == 0)//If no Silhouettes in picture ,there is nothing to remove.
            return image;
        int average = totalAmountOfBlackPixels / silhouetteCount;
        int sizeOfSmallObject = average / SIZE_OF_SMALL_OBJECT_CONSTANT;
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                if (!visited[i][j] && image[i][j]) {
                    BFSCount(image, i, j, visited);
                    if (sizeOfSmallObject > objectPixelsSize)
                        removeObject(image, i, j);
                }
            }
        }
        return image;
    }

    public static boolean[][] removeBorder(boolean[][] image, int removeLevel) {
        for (int i = 0; i < listOfSilhouettesOutlineDots.size(); i++) {
            int startX = listOfSilhouettesOutlineDots.get(i).x;
            int startY = listOfSilhouettesOutlineDots.get(i).y;
            for (int x = -removeLevel; x <= removeLevel; x++) {
                for (int y = -removeLevel; y < removeLevel; y++) {
                    int corX = startX + x;
                    int corY = startY + y;
                    if (corX < image.length && corX >= 0 && corY < image[0].length && corY >= 0 && image[corX][corY]) {
                        image[corX][corY] = false;//repaint to white
                    }
                }
            }
        }
        return image;
    }

    public static boolean[][] removeObject(boolean[][] image, int x, int y) {
        int width = image.length;
        int height = image[0].length;
        MyLinkedList<Dot> queue = new MyLinkedList<>();
        Dot dot = new Dot(x, y);
        image[x][y] = false;
        queue.offer(dot);
        while (!queue.isEmpty()) {
            Dot pulledDot = queue.poll();
            int startX = pulledDot.x;
            int startY = pulledDot.y;
            for (int i = -1; i <= 1; i++) {//Check if all 8 neighbours dots is black if yes add to the queue
                for (int j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0)
                        continue;
                    int corX = startX + i;
                    int corY = startY + j;
                    if (corX < width && corX >= 0 && corY < height && corY >= 0 && image[corX][corY]) {
                        queue.offer(new Dot(corX, corY));
                        image[corX][corY] = false;//Repaint to white color
                    }
                }
            }
        }
        return image;
    }
}
