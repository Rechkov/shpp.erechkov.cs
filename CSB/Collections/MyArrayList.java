package com.shpp.erechkov.cs.CSB.Collections;


import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by BOSS on 26.07.2016.
 */
public class MyArrayList<E> implements Iterable<E> {
    /**
     * Length of array.
     */
    private int length = 10;

    private Object[] array = new Object[length];
    /**
     * Number of elements
     */
    public int size = 0;

    public int count = 0;

    public void add(E object) {
        extendLengthIfOutOfBounds();
        array[size] = object;
        size++;
    }

    public void add(int index, E object) {
        checkIndexBounds(index);
        extendLengthIfOutOfBounds();
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = object;
        size++;
    }

    public int size() {
        return size;
    }

    public void extendLengthIfOutOfBounds() {
        if (size >= array.length) {
            length = length + length / 2;
            array = Arrays.copyOf(array, length);
        }
    }

    @SuppressWarnings("unchecked")
    public E get(int index) {
        checkIndexBounds(index);
        E e = (E) array[index];
        return e;
    }

    public void set(int index, E object) {
        checkIndexBounds(index);
        array[index] = object;
    }

    public void checkIndexBounds(int index) {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException();
    }

    public void remove(int index) {
        checkIndexBounds(index);
        int sizeToCopy = size - 1 - index;
        if (sizeToCopy != 0)
            System.arraycopy(array, index + 1, array, index, sizeToCopy);
        array[size--] = null;
    }

    public String toString() {
        String result = "[";
        for (int i = 0; i < size; i++) {
            result += String.valueOf(array[i]);
            if (i + 1 < size) {
                result += ",";
            }
        }
        return result + "]";
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<E> {
        int count = 0;

        @Override
        public boolean hasNext() {
            return count < size;
        }

        @Override
        public E next() {
            @SuppressWarnings("unchecked")
            E e = (E) array[count];
            count++;
            return e;
        }
    }
}
