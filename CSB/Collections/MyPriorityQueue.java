package com.shpp.erechkov.cs.CSB.Collections;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by BOSS on 05.08.2016.
 */
public class MyPriorityQueue<E> implements Iterable<E> {
    /**
     * Initial array length.
     */
    private int length = 11;

    /**
     * Array where elements are placed.
     */
    private Object[] queue = new Object[length];

    /**
     * The number of elements in the queue.
     */
    private int size = 0;

    /**
     * If true change default Min PriorityQueue to Max PriorityQueue.
     * To create max priority queue -  new MyPriorityQueue(true);
     */
    private boolean isMaxQueue;

    /**
     * Define the length of queue that consider to be small.
     * When queue is small coefficient to extend equals k=2.
     */
    private int SMALL_QUEUE_LENGTH = 1000;

    public MyPriorityQueue() {
        this.isMaxQueue = false;
    }

    public MyPriorityQueue(boolean setMaxQueue) {
        isMaxQueue = setMaxQueue;
    }

    @SuppressWarnings("unchecked")
    public void offer(E value) {
        extendLengthIfOutOfBounds();
        queue[size] = value;
        int n = size;
        while (true) {
            int parentIndex = (n - 1) / 2;
            boolean comparator = ((Comparable<E>) queue[n]).compareTo((E) queue[parentIndex]) < 0;
            if (isMaxQueue)
                comparator = ((Comparable<E>) queue[n]).compareTo((E) queue[parentIndex]) > 0;
            if (comparator) {
                Object temp = queue[n];
                queue[n] = queue[parentIndex];
                queue[parentIndex] = temp;
                n = parentIndex;
            } else {
                break;
            }
        }
        size++;
    }

    public void add(E value) {
        offer(value);
    }

    @SuppressWarnings("unchecked")
    public E poll() {
        if (size == 0)
            return null;
        int min;
        E returnValue = (E) queue[0];
        queue[0] = queue[size - 1];
        size--;
        int n = 0;
        while (true) {
            int leftChildIndex = 2 * n + 1;
            int rightChildIndex = leftChildIndex + 1;
            if (leftChildIndex > size || rightChildIndex > size)
                break;
            min = minOrMax(leftChildIndex, rightChildIndex);
            boolean comparator = ((Comparable<E>) queue[n]).compareTo((E) queue[min]) > 0;
            if (isMaxQueue)
                comparator = ((Comparable<E>) queue[n]).compareTo((E) queue[min]) < 0;
            if (comparator) {
                Object temp = queue[n];
                queue[n] = queue[min];
                queue[min] = temp;
                n = min;
            } else {
                break;
            }
        }
        return returnValue;
    }

    @SuppressWarnings("unchecked")
    public E peek() {
        return (size == 0) ? null : (E) queue[0];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    /**
     * If length smaller than default SMALL_QUEUE_LENGTH, queue will extend by own length(k = 2),
     * Otherwise the half of the length (k = 1.5)
     */
    public void extendLengthIfOutOfBounds() {
        if (size >= length) {
            length = length + ((length < SMALL_QUEUE_LENGTH) ? length : length / 2);
            queue = Arrays.copyOf(queue, length);
        }
    }

    @SuppressWarnings("unchecked")
    public int minOrMax(int index1, int index2) { 
        boolean comparator = ((Comparable<E>) queue[index1]).compareTo((E) queue[index2]) < 0;
        if (isMaxQueue)
            comparator = ((Comparable<E>) queue[index1]).compareTo((E) queue[index2]) > 0;
        return (comparator) ? index1 : index2;
    }

    public String toString() {
        String result = "[";
        for (int i = 0; i < size; i++) {
            result += queue[i].toString();
            if (i + 1 < size)
                result += ", ";
        }
        result += "]";
        return result;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<E> {
        int count = 0;

        @Override
        public boolean hasNext() {
            return count < size;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E next() {
            count++;
            return (E) queue[count - 1];
        }
    }
}
