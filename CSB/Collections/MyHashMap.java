package com.shpp.erechkov.cs.CSB.Collections;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by BOSS on 04.08.2016.
 */
public class MyHashMap<K, V> implements Iterable<Element<K, V>> {
    /**
     * STRUCTURE
     * A __ LINKED LIST
     * R|__| -> -> ->
     * R|__| -> ->
     * A|__| -> -> -> ->
     * Y|__| ->
     */
    /**
     * When array is filled more than this amount it's extend the length.
     */
    private double THRESHOLD_TO_EXTEND = 0.75;

    /**
     * Length of array.Do NOT depend of how many elements in array(size).
     */
    private int length = 15;

    /**
     * Number of elements  stores in MyHashMap.
     */
    private int size = 0;

    /**
     * The  element of iteration in LinkedList, will be assigned to this variable.
     */
    private Node<K, V> nextIterator;

    /**
     * The main array that contain first node of LinkedList or null if cell is empty.
     */
    @SuppressWarnings("unchecked")
    private Node<K, V>[] table = new Node[length];

    private class Node<K, V> implements Element<K, V> {
        int hash;
        K key;
        V value;
        Node<K, V> next;
        Node<K, V> previous;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(key) + ":" + String.valueOf(value);
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }
    }

    public void put(K key, V value) {
        extendLengthIfOutOfBounds();
        int hash = hash(key);
        int indexToGet = getIndex(hash, length);
        if (key == null) {//Special case when key of element is null;
            putNullKeyElement(value);
            size++;
            return;
        }
        if (table[indexToGet] == null) {//Means cell of array is free.
            Node<K, V> newEntry = new Node<>(key, value);
            newEntry.hash = hash;
            table[indexToGet] = newEntry;
            size++;
            return;
        }
        Node<K, V> nodeAtKey = getNode(key);
        if (nodeAtKey == null) {//It means no such Node with entered key in HashMap
            Node<K, V> newEntry = new Node<>(key, value);
            newEntry.hash = hash;
            newEntry.next = table[indexToGet];
            table[indexToGet].previous = newEntry;
            table[indexToGet] = newEntry;
            size++;
        } else {
            nodeAtKey.value = value;//Replace the element value
        }
    }

    public V remove(K key) {
        int indexToGet = getIndex(hash(key), length);
        Node<K, V> nodeToRemove = getNode(key);
        if (nodeToRemove == null)//Node in the HashMap was not found
            return null;
        if (nodeToRemove.previous == null) {//If Node the first element of LinkedList
            if (nodeToRemove.next != null) {//Few elements in LinkedList
                table[indexToGet] = nodeToRemove.next;
                nodeToRemove.next.previous = null;
            } else {//Only one element in LinkedList
                table[indexToGet] = null;
            }
            size--;
            return nodeToRemove.value;
        }
        if (nodeToRemove.next == null) {//If Node the last element of LinkedList
            nodeToRemove.previous.next = null;
            size--;
            return nodeToRemove.value;
        }
        nodeToRemove.previous.next = nodeToRemove.next;
        nodeToRemove.next.previous = nodeToRemove.previous;
        size--;
        return nodeToRemove.value;
    }

    public int hash(K key) {
        return Objects.hashCode(key);
    }

    public int getIndex(int hash, int length) {
        return (Math.abs(hash) % length);
    }

    public V get(K key) {
        return (getNode(key) == null) ? null : getNode(key).value;
    }

    public boolean containsKey(K key) {
        return getNode(key) != null;
    }

    private void putNullKeyElement(V value) {
        Node<K, V> newEntry = new Node<>(null, value);
        if (table[0] != null)
            table[0].previous = newEntry;
        newEntry.next = table[0];
        table[0] = newEntry;
    }

    private void extendLengthIfOutOfBounds() {
        if (size / (double) length > THRESHOLD_TO_EXTEND) {
            table = extendTable();
            length = length * 2;
        }
    }

    @SuppressWarnings("unchecked")
    private Node<K, V>[] extendTable() {
        int length = table.length * 2;
        Node<K, V>[] newTable = new Node[length];
        for (Element<K, V> element : this) {
            Node<K, V> nextNode = (Node) element;
            Node<K, V> node = new Node<>(nextNode.key, nextNode.value);
            node.hash = nextNode.hash;
            int indexToGet = getIndex(node.hash, length);
            if (node.key == null) {//Special case when key of element is null;
                if (newTable[0] != null)
                    newTable[0].previous = node;
                node.next = newTable[0];
                newTable[0] = node;
                continue;
            }
            if (newTable[indexToGet] == null) {//Means cell of array is free.
                newTable[indexToGet] = node;
                continue;
            }
            //Otherwise place Node with connection to other Node in Linked List
            node.next = newTable[indexToGet];
            newTable[indexToGet].previous = node;
            newTable[indexToGet] = node;
        }
        return newTable;
    }

    @SuppressWarnings("unchecked")
    private Node<K, V> getNode(K key) {
        int indexToGet = getIndex(hash(key), length);
        Node<K, V> firstNodeAtKey = table[indexToGet];
        if (firstNodeAtKey == null)
            return null;
        if (firstNodeAtKey.key == key || key.equals(firstNodeAtKey.key)) {
            return firstNodeAtKey;
        }
        while (firstNodeAtKey.next != null) {
            firstNodeAtKey = firstNodeAtKey.next;
            if (firstNodeAtKey.key == key || key.equals(firstNodeAtKey.key)) {
                return firstNodeAtKey;
            }
        }
        return null;
    }

    public String toString() {
        String result = "[";
        int count = 0;
        for (Element<K, V> element : this) {
            result += element;
            if (count + 1 < size) {
                result += ",";
            }
            count++;
        }
        return result + "]";
    }

    @Override
    public Iterator<Element<K, V>> iterator() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<Element<K, V>> {
        boolean isNextInArray = true;
        int countArrayIndex = 0;
        int countSize = 0;

        @Override
        public boolean hasNext() {
            return countSize < size;
        }

        @Override
        public Element<K, V> next() {
            if (isNextInArray) {//Get elements from array
                while (countArrayIndex < length) {
                    if (table[countArrayIndex] == null) {
                        countArrayIndex++;
                        continue;
                    }
                    nextIterator = table[countArrayIndex];
                    if (nextIterator.next != null)
                        isNextInArray = false;
                    countSize++;
                    countArrayIndex++;
                    break;
                }
                return nextIterator;
            } else {//Otherwise next element in LinkedList
                nextIterator = nextIterator.next;
                if (nextIterator.next == null) {
                    isNextInArray = true;
                }
                countSize++;
                return nextIterator;
            }
        }
    }
}
