package com.shpp.erechkov.cs.CSB.Collections;

import java.util.Iterator;

/**
 * Created by BOSS on 03.08.2016.
 */
public class MyLinkedList<E> implements Iterable<E> {
    /**
     * The Number of elements in the queue.
     */
    private int size;

    /**
     * First node in the LinkedList.
     */
    private Node<E> first;

    /**
     * Last node in the LinkedList.
     */
    private Node<E> last;

    private class Node<E> {
        E value;
        Node<E> previous;
        Node<E> next;

        public Node(E value, Node<E> next, Node<E> previous) {
            this.value = value;
            this.next = next;
            this.previous = previous;
        }
    }

    public void add(E value) {
        Node<E> node = new Node<>(value, null, last);
        if (first == null) {
            first = node;
            last = node;
        } else {
            last.next = node;
            last = node;
        }
        size++;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void offer(E value) {
        Node<E> node = new Node<>(value, first, null);
        if (first == null) {
            first = node;
            last = node;
        } else {
            first.previous = node;
            first = node;
        }
        size++;
    }

    public E poll() {
        if (first == null)
            return null;
        return remove(0);
    }

    public void add(E value, int index) {
        checkIndexBounds(index);
        Node<E> next = findNode(index);
        Node<E> node = new Node<>(value, null, null);
        if (next.previous == null) {
            first = node;
        } else {
            next.previous.next = node;
        }
        next.previous = node;
        node.next = next;
        node.previous = next.previous;
        size++;
    }

    public E get(int index) {
        checkIndexBounds(index);
        return findNode(index).value;
    }

    public void set(int index, E value) {
        checkIndexBounds(index);
        Node<E> node = findNode(index);
        node.value = value;
    }

    public Node<E> findNode(int index) {
        Node<E> tempNode = first;
        if (index < size / 2) {
            for (int i = 0; i < index; i++) {
                tempNode = tempNode.next;
            }
        } else {
            tempNode = last;
            for (int i = size - 1; i > index; i--) {
                tempNode = tempNode.previous;
            }
        }
        return tempNode;
    }

    public int size() {
        return size;
    }

    public void checkIndexBounds(int index) {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException();
    }

    public E remove(int index) {
        checkIndexBounds(index);
        Node<E> tempNode = findNode(index);
        Node<E> previous = tempNode.previous;
        Node<E> next = tempNode.next;
        if (previous == null) {
            first = next;
        } else {
            previous.next = next;
        }
        if (next == null)
            last = previous;
        else {
            next.previous = previous;
        }
        size--;
        return tempNode.value;
    }

    public String toString() {
        String result = "[";
        Node<E> tempNode = first;
        result += tempNode.value + ",";
        for (int i = 0; i < size; i++) {
            tempNode = tempNode.next;
            if (tempNode == last) {
                result += tempNode.value;
                break;
            }
            result += tempNode.value + ",";
        }
        return result + "]";
    }

    public Iterator<E> iterator() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<E> {
        int count = 0;
        boolean isFirstElement = true;
        Node<E> tempNode;

        @Override
        public boolean hasNext() {
            return count < size;
        }

        @Override
        public E next() {
            if (isFirstElement) {
                isFirstElement = false;
                tempNode  = first;
                count++;
                return first.value;
            }else{
                tempNode = tempNode.next;
                count++;
                return tempNode.value;
            }
        }
    }

}
