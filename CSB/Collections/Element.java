package com.shpp.erechkov.cs.CSB.Collections;

/**
 * Created by BOSS on 05.08.2016.
 */
public interface Element<K,V> {
    K getKey();
    V getValue();
}
