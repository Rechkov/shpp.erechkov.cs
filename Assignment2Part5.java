package com.shpp.erechkov.cs;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * Create optical illusion.
 * Create defenite number of black boxes that seted by NUM_ROWS and  NUM_COLS variables.
 * Whole boxes structure will be centered and divided by white lines(roads). *
 */
public class Assignment2Part5 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;
    /* The number of rows and columns in the grid, respectively. */
    private static final int NUM_ROWS = 5;
    private static final int NUM_COLS = 6;

    /* The width and height of each box. */
    private static final double BOX_SIZE = 40;

    /* The horizontal and vertical spacing between the boxes. */
    private static final double BOX_SPACING = 10;
    /*The X cordinate of center of canvas*/
    private double CENTER_X = APPLICATION_WIDTH / 2.0;
    /*The Y cordinate of center of canvas*/
    private double CENTER_Y = (APPLICATION_HEIGHT - 23) / 2.0;//23 pixels bug
    /*Define the starting X and Y location of first box(the most north western point )*/
    double firstBoxX = CENTER_X - (NUM_COLS * BOX_SIZE + (NUM_COLS - 1.0) * BOX_SPACING) / 2.0;
    double firstBoxY = CENTER_Y - (NUM_ROWS * BOX_SIZE + (NUM_ROWS - 1.0) * BOX_SPACING) / 2.0;

    public void run() {
        System.out.println(Math.round(1 / 2.0));
        System.out.println(APPLICATION_HEIGHT + " " + APPLICATION_WIDTH);
        System.out.println(CENTER_X + " " + CENTER_Y);
        for (int i = 0; i < NUM_COLS; i++) {//Create box in rows
            for (int j = 0; j < NUM_ROWS; j++) { //Create box in columns
                GRect boxY = new GRect(firstBoxX, firstBoxY, BOX_SIZE, BOX_SIZE);
                boxY.setColor(Color.BLACK);
                boxY.setFillColor(Color.BLACK);
                boxY.setFilled(true);
                add(boxY);
                firstBoxY = firstBoxY + BOX_SIZE + BOX_SPACING; //Offset box in Y direction
            }
            firstBoxX = firstBoxX + BOX_SIZE + BOX_SPACING; //Offset box in X direction
            firstBoxY = (CENTER_Y - (NUM_ROWS * BOX_SIZE + (NUM_ROWS - 1) * BOX_SPACING) / 2.0);//reset Y cordinate to starting one(Y cordinate of first row)
        }
    }
}