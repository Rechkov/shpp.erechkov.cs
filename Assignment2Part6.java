package com.shpp.erechkov.cs;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * Created by BOSS on 04.05.2016.
 */
public class Assignment2Part6 extends WindowProgram {
    public static int NUMBER_OF_SGEMENTS = 20;
    public static double SEGMENT_DIAMETER = 50;
    public static double SEGMENT_OFFSET_X = SEGMENT_DIAMETER / 4.0;
    public static double SEGMENT_OFFSET_Y = SEGMENT_DIAMETER / 3.0;
    public static double DOUBLE_APPLICATION_WIDTH = NUMBER_OF_SGEMENTS * SEGMENT_DIAMETER - (NUMBER_OF_SGEMENTS - 1) * SEGMENT_OFFSET_X;
    public static double DOUBLE_APPLICATION_HEIGHT = SEGMENT_DIAMETER + (SEGMENT_DIAMETER / 3.0);
    public static final int APPLICATION_WIDTH = (int) Math.round(DOUBLE_APPLICATION_WIDTH);//Can create deviation but not more than 0.5 pixel
    public static final int APPLICATION_HEIGHT = (int) Math.round(DOUBLE_APPLICATION_HEIGHT) + 23;//23 pixel of bug

    //public double APPLICATION_HEIGHT=SEGMENT_RADIUS+SEGMENT_RADIUS/3.0;
    public void run() {
        drawSegments();
    }
    public void drawSegments() {
        double locationX = 0;
        double locationY;
        for (int i = 0; i < NUMBER_OF_SGEMENTS; i++) {
            double y;
            if (i % 2 == 0) {
                locationY = SEGMENT_OFFSET_Y;
            } else {
                locationY = 0;
            }
            GOval circle = new GOval(locationX, locationY, SEGMENT_DIAMETER, SEGMENT_DIAMETER);
            circle.setColor(Color.RED);
            circle.setFilled(true);
            circle.setFillColor(Color.GREEN);
            add(circle);
            locationX = locationX + SEGMENT_DIAMETER - SEGMENT_OFFSET_X;//Offset to the right of the screen

        }
    }

}
