package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

/**
 * Created by BOSS on 17.05.2016.
 */
public class Assignment5Part1 extends TextProgram {
    int count;
    public void run() {
        /* Repeatedly prompt the user for a word and print out the estimated
         * number of syllables in that word.
         */
        while (true) {
            String word = readLine("Enter a single word: ");
            println("  Syllable count: " + syllablesIn(word));
        }
    }

    /**
     * Given a word, estimates the number of syllables in that word according to the
     * heuristic specified in the handout.
     *
     * @param word A string containing a single word.
     * @return An estimate of the number of syllables in that word.
     */
    private int syllablesIn(String word) {
        count = 0;
        countVowels(word);
        if (word.charAt(word.length() - 1) == 'e' && word.charAt(word.length() - 2) != 'e') { //Special case: if vowel "e" is the last
            count--;                                                                          // number of syllable is decreased
        }
        if (count < 1) {
            return 1;
        } else {
            return count;
        }
    }

    /**
     * Count vowel without repetition. For example if 'aa' in a row it counts like one vowel.
     * @param word A string that contains a single word.
     */
    private void countVowels(String word){
        for (int i = 0; i < word.length(); i++) { // Find vowel and increase count by 1
            char ch = word.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'y' || ch == 'A' || ch == 'E' ||
                    ch == 'I' || ch == 'O' || ch == 'U' || ch == 'Y') {
                count++;
                for (int j = i + 1; j < word.length(); j++) {//Find next vowels in a row relatively to first found vowel
                    ch = word.charAt(j);                     // and make main  for i loop to skip it by increasing i;
                    if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'y' || ch == 'A' || ch == 'E' ||
                            ch == 'I' || ch == 'O' || ch == 'U' || ch == 'Y') {
                        i++;
                    } else {
                        break;
                    }
                }
            }
        }
    }

}

