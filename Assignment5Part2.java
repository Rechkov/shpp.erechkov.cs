package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

/**
 * Created by BOSS on 20.05.2016.
 */
public class Assignment5Part2 extends TextProgram {
    String max;//String with highest length
    String min;


    public void run() {
        /* Sit in a loop, reading numbers and adding them. */
        while (true) {
            String n1 = readLine("Enter first number:  ");
            String n2 = readLine("Enter second number: ");
            println(n1 + " + " + n2 + " = " + addNumericStrings(n1, n2));
            println();
        }
    }

    /**
     * Given two string representations of nonnegative integers, adds the
     * numbers represented by those strings and returns the result.
     *
     * @param n1 The first number.
     * @param n2 The second number.
     * @return A String representation of n1 + n2
     */
    private String addNumericStrings(String n1, String n2) {
        findLongestString(n1, n2);
        return SumValuesOfStrings();
    }

    private void findLongestString(String n1, String n2) {
        if (n1.length() > n2.length()) {
            max = n1;
            min = n2;
        } else {
            max = n2;
            min = n1;
        }
    }

    /**
     * Sum each element of two string
     *
     * @return A String representation of n1 + n2
     */
    private String SumValuesOfStrings() {
        int value1;
        int value2;
        int temp_value = 0;
        String result = "";
        int sum;
        for (int i = max.length() - 1 ; i >= 0 ; i--) { //Loop threw each character of 2 String from right to left
            value1 = max.charAt(i) - '0';
            if(max.length() -  min.length() <= i){
                value2 = min.charAt(i - (max.length() -  min.length())) - '0';
            }else{
                value2 = 0;//If shortest string out of range value2 will be equaled to 0;
            }
            sum = value1 + value2 + temp_value;
            if(sum >=10){
                sum = sum -10;
                temp_value = 1; //Like "One in mind" in real life calculation
            }else{
                temp_value = 0;
            }
            char ch  = (char) (sum + '0');
            result = ch + result;
        }
        if (temp_value == 1){      //If last calculation has "One in mind" it add "1" to left part of result string
            result =  "1" + result;
        }
        return result;
    }
}
