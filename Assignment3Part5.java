package com.shpp.erechkov.cs;

import java.util.Random;

import com.shpp.cs.a.console.TextProgram;

/**
 * Created by BOSS on 05.05.2016.
 */
public class Assignment3Part5 extends TextProgram {
    public void run() {
        Random dice = new Random();
        int flip_coins = dice.nextInt(2);
        int total_earned = 0;
        int earned = 0;
        while (total_earned <= 20) {
            while (flip_coins == 0) {
                flip_coins = dice.nextInt(2);
                earned++;
            }
            flip_coins = dice.nextInt(2);//renew flip_coins value for second while loop
            total_earned = total_earned + earned;
            System.out.println("This game you earned " + earned);
            System.out.println("Your total is " + total_earned);
        }
    }

}
