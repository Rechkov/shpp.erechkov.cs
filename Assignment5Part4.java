package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by BOSS on 22.05.2016.
 */
public class Assignment5Part4 extends TextProgram {

    boolean decision = true;
    ArrayList<String> result = new ArrayList<>();

    public void run() {

        System.out.println(extractColumn("files/test_csv - Лист1.csv", 2));
    }

    private ArrayList<String> extractColumn(String filename, int columnIndex) {
        getColumnValues(filename, columnIndex);
        toNormalView();
        return result;
    }

    private void changeDecision() {
        if (decision) {
            decision = false;
        } else {
            decision = true;
        }
    }

    private BufferedReader openFile(String fileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            return br;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get values between comas for requested column including all additional quotes.
     *
     * @param filename
     * @param columnIndex
     */
    private void getColumnValues(String filename, int columnIndex) {
        try {
            BufferedReader br = openFile(filename);
            while (true) { //Read file line by line until lines exist.
                int counter = 0;
                int start_index = 0;
                int end_index = 0;
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                for (int i = 0; i < line.length(); i++) {//Loop threw the line and find start and end index of requested column,comas exclusively
                    if (line.charAt(i) == '"') { //If find quote first time, comas are not taken into account.
                        changeDecision();        //If quote pair is found works as normal.
                    }
                    if (line.charAt(i) == ',' && decision || i == line.length() - 1) {//if coma or it's last element of line.
                        if (columnIndex == 0) {
                            start_index = 0;    //To avoid bug when first element of first column do not count.
                        } else {
                            start_index = end_index + 1;
                        }
                        if (line.length() - 1 == i) {
                            end_index = i + 1;  //To avoid bug when last element of last column do not count.
                        } else {
                            end_index = i;
                        }
                        counter++;
                        if (counter > columnIndex) {//Stop looping when reached requested column.
                            break;
                        }
                    }
                }
                result.add(line.substring(start_index, end_index));
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Rid of unnecessary(additional) quotes.
     */
    private void toNormalView() {
        String tempValue;
        for (int i = 0; i < result.size(); i++) {
            String line = result.get(i);
            if (result.get(i).charAt(0) == '"') {
                tempValue = result.get(i).substring(1, line.length() - 1);
                tempValue = tempValue.replace("\"\"", "\"");
                result.set(i, tempValue);
            }
        }
    }

}



