package com.shpp.erechkov.cs;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * Created by BOSS on 02.05.2016.
 */
public class Assignment2Part4 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;
    public static  double FLAG_HEIGHT = 250;
    public static  double FLAG_WIDTH = 325;
    private static final Color CARDINAL_RED = new Color(196, 30, 58);
    /* Determine the coordinates of the center of the window. */

    public void run() {
        double xCenter = getWidth() / 2.0;
        double yCenter = getHeight() / 2.0;
        GLabel flag_name = new GLabel("Flag of belgium");
        flag_name.setLocation(getWidth() - flag_name.getWidth(), getHeight() - flag_name.getDescent());
        add(flag_name);
        GRect first_sector = new GRect(xCenter - FLAG_WIDTH / 2.0,yCenter - FLAG_HEIGHT / 2.0, FLAG_WIDTH / 3.0, FLAG_HEIGHT);
        first_sector.setFilled(true);
        first_sector.setColor(Color.black);
        first_sector.setFillColor(Color.black);
        add(first_sector);
        GRect second_sector = new GRect(xCenter  - FLAG_WIDTH / 6.0, yCenter - FLAG_HEIGHT / 2.0, FLAG_WIDTH / 3.0, FLAG_HEIGHT);
        second_sector.setFillColor(Color.YELLOW);
        second_sector.setFilled(true);
        second_sector.setColor(Color.YELLOW);
        add(second_sector);
        GRect third_sector = new GRect(xCenter  + FLAG_WIDTH / 6.0,yCenter - FLAG_HEIGHT / 2.0, FLAG_WIDTH / 3.0, FLAG_HEIGHT);
        third_sector.setFilled(true);
        third_sector.setColor(CARDINAL_RED);
        third_sector.setFillColor(CARDINAL_RED);
        add(third_sector);
        }
}
