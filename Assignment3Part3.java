package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

/**
 * Created by BOSS on 05.05.2016.
 */
public class Assignment3Part3 extends TextProgram {
    public void run() {
        System.out.println(raiseToPower(0.3, -3));
        System.out.println(Math.pow(0.3, -3));
    }

    private double raiseToPower(double base, int exponent) {
        double result = 1;
        for (int i = 0; i < abs(exponent); i++) {
            result = result * base;
        }
        if (exponent > 0) {
            return result;
        } else if (exponent < 0) {
            return 1 / result;
        } else
            return 1;
    }

    /**
     * @param a take int number
     * @return a number on module(only positive)
     */
    public int abs(int a) {
        if (a > 0) {
            return a;
        } else {
            return -a;
        }
    }
}
