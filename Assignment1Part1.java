package com.shpp.erechkov.cs;
import com.shpp.karel.KarelTheRobot;
/**
 * Pick the newspaper
 * Created by BOSS on 28.04.2016.
 */
public class Assignment1Part1 extends KarelTheRobot {
    public void run() throws Exception {
        goForNewspaper();
        pickBeeper();
        goHome();
    }
    public void goHome() throws Exception{
        turnAround();
        while (frontIsClear()){
            move();
        }
        turnRight();
        move();
        turnRight();
    }
    public void goForNewspaper() throws Exception{
        turnRight();
        move();
        turnLeft();
        while (noBeepersPresent()){
            move();
        }
    }
    public void turnRight() throws Exception{
        turnLeft();
        turnLeft();
        turnLeft();
    }
    public void turnAround() throws Exception{
        turnLeft();
        turnLeft();
    }
}
