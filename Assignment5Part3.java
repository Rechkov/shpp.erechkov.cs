package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BOSS on 22.05.2016.
 */
public class Assignment5Part3 extends TextProgram {

    private int numberOfMatchedLetters;

    public void run() {
        try {
            List<String> lines = Files.readAllLines(Paths.get("files/dictionary.txt"), StandardCharsets.UTF_8);//stackoverflow
            while (true) {
                String letters3 = enter3Letters();//Prompt to enter 3 letters
                for (int i = 0; i < lines.size() ; i++) {
                    String line = lines.get(i);
                    countNumberOfMatchedLetters(line, letters3);
                    printMatchedWord(line);
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private void printMatchedWord(String line) {
        if (numberOfMatchedLetters >= 3) {
            System.out.println(line);
        }
    }

    /**
     * Find macthes between entred letters and word in dicationary until number of matches
     * become more than 2.
     *
     * @param line     its word in dictionary
     * @param letters3 entered letters
     */
    private void countNumberOfMatchedLetters(String line, String letters3) {
        numberOfMatchedLetters = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == letters3.charAt(numberOfMatchedLetters)) {
                numberOfMatchedLetters++;
                if (numberOfMatchedLetters > 2) {
                    break;
                }
            }
            if (i >= line.length()) {
                break;
            }
        }
    }

    private String enter3Letters() {
        String letters3 = readLine("Enter 3 letter: ");
        letters3 = letters3.toLowerCase();
        return letters3;
    }
}