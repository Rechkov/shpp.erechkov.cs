package com.shpp.erechkov.cs;

import acm.graphics.*;
import acm.util.RandomGenerator;
import com.shpp.cs.a.console.TextProgram;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.event.*;
import java.awt.Color;
import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Breakout game  extend version.
 * Extensions:
 * -Speed increasing
 * -Life label
 * -Add score
 */

public class Assignment4Part2 extends WindowProgram {

    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 600;


    /**
     * Dimensions of game board (usually the same)
     */
    private static final int WIDTH = APPLICATION_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT;

    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final int BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static int NTURNS = 3;
    /**
     * The short amount of time when while loop will be stopped
     */
    private static int PAUSE_TIME = 5;

    GRect paddle;
    double paddleX;
    GOval ball;
    int NUMBER_OF_BRICKS = NBRICK_ROWS * NBRICKS_PER_ROW;
    double vx;
    double vy;
    int score_points = 0;
    GLabel score;
    long startContactTimePeriod;
    long endContactTimePeriod;
    GRect bonus;
    GObject object;
    Thread t1;
    GLabel timeRemain;
    GLabel lifes;
    int oneSecond = 1000;//in milliseconds
    /**
     * The x cordinate of the ball
     */
    double ballX;
    /**
     * The y cordinate of the ball
     */
    double ballY;

    Color defaultColor = Color.black;

    int DIAMETER = 2 * BALL_RADIUS;

    private GLabel clickToContinue;

    private RandomGenerator rgen = RandomGenerator.getInstance();

    boolean outOfPedalContact = true;//if contact between ball and paddle not exist equals to true

    boolean isPaddleWider = false;

    int randomBonus;

    public void run() {
        createWorld();
        runGame();
    }

    public void createWorld() {
        //remove(paddle);//Randomly created speed and direction of a
        createPaddle();
        drawWallOfBricks();
        createBall();
        ballRandomSpeed();
        score(score_points);
        displayLifeRemaining();
        addMouseListeners();
    }

    public void runGame() {
        while (NTURNS > 0) {
            runGame1Time();
            if (NTURNS < 1) {
                displayMessageText("You lose.");
                break;
            }
            if (NUMBER_OF_BRICKS < 1) {
                displayMessageText("You win!");
                break;
            }
            displayMessageClickToContinue();
            waitForClick();
            remove(clickToContinue);
            createPaddle();
            ballRandomSpeed();
        }
    }

    /**
     * Create random ball speed and direction and set ball to the center of the screen
     */
    public void ballRandomSpeed() {
        vx = rgen.nextDouble(0.5, 1);
        if (rgen.nextBoolean(0.5))
            vx = -vx;
        vy = rgen.nextDouble(0.5, 1);
        if (rgen.nextBoolean(0.5))
            vy = -vy;
        ball.setLocation(APPLICATION_WIDTH / 2.0 - BALL_RADIUS + vx, APPLICATION_HEIGHT / 2.0 + vy);
    }

    public void score(int points) {
        score = new GLabel("Score: " + points);
        score.setLocation((APPLICATION_WIDTH - score.getWidth()) / 2, getHeight() - score.getDescent());
        score.setFont("London-18");
        score.setColor(defaultColor);
        add(score);
    }

    public int countScore() {
        if (getCollidingObject().getColor() != Color.CYAN) {
            score_points += 100;
        }
        return score_points += 100;
    }

    public void displayLifeRemaining() {
        if (lifes != null) {
            remove(lifes);
        }
        lifes = new GLabel("LIFES  X  " + NTURNS);
        lifes.setLocation((APPLICATION_WIDTH - lifes.getWidth()) - DIAMETER * 2, getHeight() - lifes.getDescent());
        lifes.setFont("London-18");
        lifes.setColor(defaultColor);
        add(lifes);
    }

    public void drawWallOfBricks() {
        Color[] color = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN};
        double startX = (APPLICATION_WIDTH - ((NBRICKS_PER_ROW * BRICK_WIDTH) + (BRICK_SEP * NBRICKS_PER_ROW - 1))) / 2.0;
        double startY = BRICK_Y_OFFSET;
        double temp_startX = startX;
        //Draw wall of bricks
        for (int i = 0; i < NBRICK_ROWS; i++) {
            for (int j = 0; j < NBRICKS_PER_ROW; j++) {
                if (getElementAt(startX, startY) == null) {
                    GRect brick = new GRect(startX, startY, BRICK_WIDTH, BRICK_HEIGHT);
                    brick.setColor(color[i / 2]);
                    brick.setFilled(true);
                    brick.setFillColor(color[i / 2]);
                    add(brick);
                    startX = startX + BRICK_WIDTH + BRICK_SEP;
                }
            }
            startY = startY + BRICK_HEIGHT + BRICK_SEP;
            startX = temp_startX;
        }
    }

    private GObject getCollidingObject() {
        ballX = ball.getX();
        ballY = ball.getY();
        //Return object which interact with the ball
        GObject el;
        el = getElementAt(ballX, ballY);
        if (el != null && el != bonus) {
            return el;
        }
        el = getElementAt(ballX, ballY + DIAMETER);
        if (el != null && el != bonus) {
            return el;
        }
        el = getElementAt(ballX + DIAMETER, ballY);
        if (el != null && el != bonus) {
            return el;
        }

        el = getElementAt(ballX + DIAMETER, ballY + DIAMETER);
        if (el != null && el != bonus) {
            return el;
        }
        el = getElementAt(ballX - 1, ballY + BALL_RADIUS);//-1 here to avoid situation when ball return as object
        if (el != null && el != bonus) {
            return el;
        }
        el = getElementAt(ballX + DIAMETER + 1, ballY + BALL_RADIUS);
        if (el != null && el != bonus) {
            return el;
        }
        return null;
    }

    public void createBall() {
        ball = new GOval(DIAMETER, DIAMETER);
        ball.setFilled(true);
        ball.setFillColor(defaultColor);
        ball.setColor(defaultColor);
        add(ball);
    }

    public void createPaddle() {
        if (paddle != null) {
            paddleX = paddle.getX();
            remove(paddle);
        }
        isPaddleWider = false;
        paddle = new GRect(paddleX, getHeight() - PADDLE_Y_OFFSET, PADDLE_WIDTH, PADDLE_HEIGHT);
        paddle.setFillColor(defaultColor);
        paddle.setColor(defaultColor);
        paddle.setFilled(true);
        add(paddle);
    }

    public void runGame1Time() {
        displayLifeRemaining();
        while (true) {
            ballX = ball.getX();
            ballY = ball.getY();
            object = getCollidingObject();
            paddleContactLogic();
            brickContactLogic();
            moveBallAndBonuses();
            applyBonusLogic();
            pause(PAUSE_TIME);
            //Stop the game and decrease number of life when ball hits bottom of canvas
            if (ballY + DIAMETER >= getHeight() + BALL_RADIUS) {
                vy = -vy;
                NTURNS--;
                displayLifeRemaining();
                if (bonus != null && timeRemain != null) {
                    remove(bonus);
                    bonus = null;
                    remove(timeRemain);
                }
                break;
            }
            //Win the game break
            if (NUMBER_OF_BRICKS < 1) {
                break;
            }
            vy *= 1.0001;//Speed up the game continuously
            vx *= 1.0001;
        }
    }

    /**
     * Move ball in x and y direction with speed vx and vy, and also control ball reflection
     * from canvas sides.
     */
    public void moveBallAndBonuses() {
        ballX = ball.getX();
        ballY = ball.getY();
        if (ballX <= 0 || ballX + DIAMETER >= APPLICATION_WIDTH) {
            vx = -vx;
        }
        if (ballY <= 0) {
            vy = -vy;
        }
        ball.setLocation(ballX + vx, ballY + vy);
        if (bonus != null) {
            bonus.setLocation(bonus.getX(), bonus.getY() + 1.5);//1.5 the speed of bonus
        }
        if (bonus != null) {
            if (bonus.getY() > getHeight()) {
                remove(bonus);
                bonus = null;
            }
        }
    }

    public void mouseMoved(MouseEvent e) {
        double paddle_X;
        double last_paddle_Y = paddle.getLocation().getY();
        if (e.getX() >= APPLICATION_WIDTH - PADDLE_WIDTH) {
            paddle_X = APPLICATION_WIDTH - PADDLE_WIDTH;
        } else {
            paddle_X = e.getX();
        }
        paddle.setLocation(paddle_X, last_paddle_Y);
    }

    public void displayMessageText(String text) {
        GLabel loser = new GLabel(text);
        loser.setLocation((APPLICATION_WIDTH - loser.getWidth()) / 2, getHeight() / 2);
        loser.setFont("London-24");
        loser.setColor(defaultColor);
        add(loser);
    }


    public void displayMessageClickToContinue() {
        if(clickToContinue != null){
            remove(clickToContinue);
            clickToContinue = null;
        }
        if(clickToContinue == null) {
            clickToContinue = new GLabel("CLICK TO CONTINUE");
            clickToContinue.setColor(defaultColor);
            clickToContinue.setFont("London-18");
            clickToContinue.setLocation((APPLICATION_WIDTH - clickToContinue.getWidth()) / 2, getHeight() / 2);
            add(clickToContinue);
        }

    }

    public void makePaddleWider() {
        paddleX = paddle.getX();
        remove(paddle);
        isPaddleWider = true;
        paddle = new GRect(paddleX, getHeight() - PADDLE_Y_OFFSET, PADDLE_WIDTH + 20, PADDLE_HEIGHT);
        paddle.setFillColor(defaultColor);
        paddle.setColor(defaultColor);
        paddle.setFilled(true);
        add(paddle);
    }

    public void applyBonusLogic() {
        if (bonus != null) {
            boolean checkBonusHitThePaddle = checkBonusHitThePaddle();
            if ( checkBonusHitThePaddle && randomBonus == 1) {//If bonus hit the paddle it become wider for 10 seconds
                displayCountDown();
                remove(bonus);
                bonus = null;
                makePaddleWider();
            } else if (checkBonusHitThePaddle && randomBonus == 2) {
                remove(bonus);
                bonus = null;
                massiveRepaint();
            } else if (checkBonusHitThePaddle && randomBonus == 3) {
                remove(bonus);
                bonus = null;
                NTURNS++;
                displayLifeRemaining();
            }
        }
        if (t1 != null) {
            if (!t1.isAlive() && isPaddleWider) {
                createPaddle();
            }
        }
    }


    public void createRandomBonus() {
        Color GOLD = new Color(255, 200, 0);
        randomBonus = rgen.nextInt(1, 3);
        if ((randomBonus == 1 || randomBonus == 15 || randomBonus == 14) && bonus == null) {// 1/5 chance
            randomBonus = 1;
            createBonus(Color.BLUE);
        }
        if ((randomBonus == 2 || randomBonus == 12) && bonus == null) {// 2/15 chance
            randomBonus = 2;
            createBonus(defaultColor);
        }
        if (randomBonus == 3 && bonus == null) {
            randomBonus = 3; // 1/15 chance
            createBonus(GOLD);
        }
    }

    public void createBonus(Color color) {
        bonus = new GRect(ball.getX(), ball.getY(), 10, 10);
        bonus.setFilled(true);
        bonus.setFillColor(color);
        bonus.setColor(color);
        add(bonus);
    }


    public boolean checkBonusHitThePaddle() {
        if (getElementAt(bonus.getX() + bonus.getWidth(), bonus.getY() + bonus.getHeight()) == paddle) {
            return true;
        }
        if (getElementAt(bonus.getX(), bonus.getY()+bonus.getHeight()) == paddle) {
            return true;
        }
        if (getElementAt(bonus.getX(), bonus.getY()) == paddle) {
            return true;
        }
        if (getElementAt(bonus.getX() + bonus.getHeight(), bonus.getY()) == paddle) {
            return true;
        }

        return false;
    }


    public void displayCountDown() {
        if (t1 == null) {
            threadCountDown();
        }
        if (!t1.isAlive()) {
            threadCountDown();
        }
    }

    public void threadCountDown() {
        t1 = new Thread(() -> {
            for (int i = 10; i > 0; i--) {
                try {
                    if (timeRemain != null) {
                        remove(timeRemain);
                    }
                    timeRemain = new GLabel(" :" + i);
                    timeRemain.setLocation(10, getHeight() - timeRemain.getDescent());
                    timeRemain.setFont("London-24");
                    timeRemain.setColor(defaultColor);
                    add(timeRemain);
                    Thread.sleep(1000);
                    if (i == 1) {
                        remove(timeRemain);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
            }
            // code goes here.
        });
        t1.start();
    }

    public void massiveRepaint() {
        if (defaultColor == Color.BLACK) {
            defaultColor = Color.WHITE;
            setBackground(Color.BLACK);
        } else {
            defaultColor = Color.BLACK;
            setBackground(Color.WHITE);
        }

        ball.setColor(defaultColor);
        ball.setFillColor(defaultColor);
        paddle.setFillColor(defaultColor);
        paddle.setColor(defaultColor);
        if (timeRemain != null) {
            timeRemain.setColor(defaultColor);
        }
        score.setColor(defaultColor);
        lifes.setColor(defaultColor);

    }

    public void brickContactLogic() {
        if (object != null) { //While hit the bricks remove it and decrease total number of it
            if (object.getColor() != defaultColor) {
                vy = -vy;
                remove(score);
                score(countScore());
                remove(object);
                if (t1 == null) {  //Drop the bonus when ball hit the brick
                    createRandomBonus(); //Create bonus first time
                } else {
                    if (!t1.isAlive()) {//Do not create another bonus until previous is active
                        createRandomBonus();
                    }
                }
                NUMBER_OF_BRICKS--;
            }
        }
    }

    public void paddleContactLogic() {
        if (object == paddle) {
            //Reverse the ball in both direction of it hit one of paddle side
            if (vy > 0) {
                vy = -vy;
            }
            endContactTimePeriod = System.currentTimeMillis();
            if (endContactTimePeriod - startContactTimePeriod > oneSecond) {
                outOfPedalContact = true;
            }
            if (ball.getY() + DIAMETER - 4 > paddle.getY()) { // -4 pixels to be sure vx will be changed only in case
                if (outOfPedalContact) {                      // ball hit the side of paddle
                    vx = -vx;
                    outOfPedalContact = false;
                    startContactTimePeriod = System.currentTimeMillis();
                }
            }
        }
    }
}



