package com.shpp.erechkov.cs;

import com.shpp.cs.a.graphics.WindowProgram;
import acm.graphics.*;

import java.awt.*;

/**
 * Created by BOSS on 05.05.2016.
 */
public class Assignment3Part4 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;
    int BRICK_WIDTH = 20;
    int BRICK_HEIGHT = 10;
    int BRICKS_IN_BASE = 19;
    double startX = (APPLICATION_WIDTH - BRICK_WIDTH * BRICKS_IN_BASE) / 2.0;//X coordinate first box of lowest row of pyramid.
    double startY = APPLICATION_HEIGHT - BRICK_HEIGHT - 23;//Y coordinate first brick.

    public void run() {
        System.out.println(getWidth() / 2 + "   " + APPLICATION_WIDTH);
        double temp_startX = startX;//Remember start position because variable startX will be changed
        /* Draw the row of bricks starting from lower one.*/
        for (int i = 0; i < BRICKS_IN_BASE; i++) {
            for (int j = 0; j < BRICKS_IN_BASE - i; j++) {
                GRect brick = new GRect(startX, startY, BRICK_WIDTH, BRICK_HEIGHT);
                brick.setColor(Color.black);
                brick.setFilled(true);
                brick.setFillColor(Color.BLUE);
                add(brick);
                startX = startX + BRICK_WIDTH;
            }
            startX = temp_startX + BRICK_WIDTH * (i + 1) / 2;//Reset X including offset half of brick.
            startY = startY - BRICK_HEIGHT;//Y go higher in brick height.
        }

    }
}
