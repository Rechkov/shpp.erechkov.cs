package com.shpp.erechkov.cs;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.Color;
import java.awt.geom.GeneralPath;

/**
 * Created by BOSS on 03.05.2016.
 */
public class Assignment2Mycreativity1 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 700;
    public static final int APPLICATION_HEIGHT = 500;
    public static final int EARTH_WIDTH = 100;
    public static final int EARTH_HEIGHT = 100;
    static Color LILU = new Color(0, 255, 255);

    public void run() {
        GOval Earth = new GOval(getWidth() / 2 - EARTH_WIDTH / 2, getHeight() / 2 - EARTH_HEIGHT / 2, EARTH_WIDTH, EARTH_HEIGHT);
        Earth.setFilled(true);
        Earth.setFillColor(LILU);
        Earth.setColor(Color.blue);
        add(Earth);
        GOval Erth_Center = new GOval(getWidth() / 2, getHeight() / 2, 3, 3);
        Erth_Center.setFillColor(Color.black);
        Erth_Center.setFilled(true);
        add(Erth_Center);
        GOval Moon = new GOval(getWidth() / 2 - EARTH_WIDTH / 8, getHeight() / 2 - EARTH_HEIGHT * 2 + EARTH_HEIGHT / 8, EARTH_WIDTH / 4, EARTH_HEIGHT / 4);
        Moon.setFilled(true);
        Moon.setFillColor(Color.BLACK);
        Moon.setColor(Color.GRAY);
        add(Moon);
        double x = 180;
        double y = 3;
        double theta = 0.00001;
        while (true) {
            x = x + 1 + theta;
            theta += 0.001;
            Moon.movePolar(3, x);
            pause(10);
            double a = Moon.getLocation().getX();
            double b = Moon.getLocation().getY();
            //System.out.println(a+" "+b);
        }
    }
}
