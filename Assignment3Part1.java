package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

import java.util.Scanner;
/**
 * Created by BOSS on 05.05.2016.
 */
public class Assignment3Part1 extends TextProgram {
    public void run() {
        /**How many days of 30 minutes aerobic was  */
        int MoreThan30Count  =0;
        /**How many days of 40 minutes aerobic was  */
        int MoreThan40Count  =0;
        Scanner scan = new Scanner(System.in);
        int[] week_data = new int[7];
        for (int i = 0; i < 7; i++) {
            System.out.print("How many minutes did you do on day " +(i + 1)+ "? ");
            week_data[i]=Integer.parseInt(scan.nextLine());
        }
        for (int i = 0; i <7 ; i++) {
            if(week_data[i] >= 30){
                MoreThan30Count++;
            }
            if(week_data[i] >=40){
                MoreThan40Count++;
            }
        }
        if(MoreThan30Count >= 5){
            System.out.println("Cardiovacular health:");
            System.out.println("\tGreat job! You've done enough exercise for cardiovacular health.");
        }else{
            System.out.println("Cardiovacular health:");
            System.out.println("\tYou needed to train hard for at least" + (5 - MoreThan30Count) +" more day(s) a week!");
        }
        if(MoreThan40Count >= 3){
            System.out.println("Blood pressure:");
            System.out.println("\tGreat job! You've done enough exercise to keep a low blood pressure.");
        }else{
            System.out.println("Blood pressure:");
            System.out.println("You needed to train hard for at leastv " +(3 - MoreThan40Count) + " more day(s) a week!");
        }

    }
}
