package com.shpp.erechkov.cs;

import acm.graphics.*;
import acm.util.RandomGenerator;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.event.*;
import java.awt.Color;

/**
 * Breakout game  standard version.
 */

public class Assignment4Part1 extends WindowProgram {

    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 600;

    /**
     * Dimensions of game board (usually the same)
     */
    private static final int WIDTH = APPLICATION_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT;

    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 8;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final int BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static int NTURNS = 3;

    GRect paddle;
    GOval ball;
    int NUMBER_OF_BRICKS = NBRICK_ROWS * NBRICKS_PER_ROW;//Total number of bricks
    double vx;
    double vy;

    public void run() {
        createWorld();
        while (NTURNS > 0) {
            runGame1Time();
            if (NTURNS < 1) {
                GLabel loser = new GLabel("You lose.");
                loser.setLocation((APPLICATION_WIDTH - loser.getWidth()) / 2, APPLICATION_HEIGHT / 2);
                loser.setFont("London-24");
                add(loser);
                break;
            }
            if (NUMBER_OF_BRICKS < 1) {
                GLabel winner = new GLabel("Winner!");
                winner.setLocation((APPLICATION_WIDTH - winner.getWidth()) / 2, APPLICATION_HEIGHT / 2);
                winner.setFont("London-24");
                add(winner);
                break;
            }
            waitForClick();
            ballRandomSpeed();
        }
    }

    public void createWorld() {
        paddle();
        drawWallOfBricks();
        ball();
        ballRandomSpeed();
        addMouseListeners();
    }

    /**
     * Create random ball speed and direction and set ball to the center of the screen
     */
    public void ballRandomSpeed() {
        RandomGenerator rgen = RandomGenerator.getInstance();
        vx = rgen.nextDouble(1.0, 3.0);
        if (rgen.nextBoolean(0.5))
            vx = -vx;
        vy = rgen.nextDouble(1.0, 3.0);
        if (rgen.nextBoolean(0.5))
            vy = -vy;
        ball.setLocation(APPLICATION_WIDTH / 2.0 - BALL_RADIUS + vx, APPLICATION_HEIGHT / 2.0 - BALL_RADIUS + vy);
    }

    public void drawWallOfBricks() {
        Color[] color = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN};
        double startX = (APPLICATION_WIDTH - ((NBRICKS_PER_ROW * BRICK_WIDTH) + (BRICK_SEP * NBRICKS_PER_ROW - 1))) / 2.0;
        double startY = BRICK_Y_OFFSET;
        double temp_startX = startX;
        //Draw wall of bricks
        for (int i = 0; i < NBRICK_ROWS; i++) {
            for (int j = 0; j < NBRICKS_PER_ROW; j++) {
                if (getElementAt(startX, startY) == null) {
                    GRect brick = new GRect(startX, startY, BRICK_WIDTH, BRICK_HEIGHT);
                    brick.setColor(color[i / 2]);
                    brick.setFilled(true);
                    brick.setFillColor(color[i / 2]);
                    add(brick);
                    startX = startX + BRICK_WIDTH + BRICK_SEP;
                }
            }
            startY = startY + BRICK_HEIGHT + BRICK_SEP;
            startX = temp_startX;
        }
    }

    private GObject getCollidingObject() {
        if (getElementAt(ball.getX(), ball.getY()) != null) {
            return getElementAt(ball.getX(), ball.getY());
        } else if (getElementAt(ball.getX() + 2 * BALL_RADIUS, ball.getY()) != null) {
            return getElementAt(ball.getX() + 2 * BALL_RADIUS, ball.getY());
        } else if (getElementAt(ball.getX(), ball.getY() + 2 * BALL_RADIUS) != null) {
            return getElementAt(ball.getX(), ball.getY() + 2 * BALL_RADIUS);
        } else if (getElementAt(ball.getX() + 2 * BALL_RADIUS, ball.getY() + 2 * BALL_RADIUS) != null) {
            return getElementAt(ball.getX() + 2 * BALL_RADIUS, ball.getY() + 2 * BALL_RADIUS);
        } else {
            return null;
        }
    }

    public void ball() {
        ball = new GOval(BALL_RADIUS * 2, BALL_RADIUS * 2);
        ball.setFilled(true);
        ball.setFillColor(Color.BLACK);
        ball.setColor(Color.BLACK);
        add(ball);
    }

    public void paddle() {
        paddle = new GRect(0, APPLICATION_HEIGHT - PADDLE_Y_OFFSET - 23, PADDLE_WIDTH, PADDLE_HEIGHT);
        paddle.setFillColor(Color.BLACK);
        paddle.setColor(Color.BLACK);
        paddle.setFilled(true);
        add(paddle);
    }

    public void runGame1Time() {
        while (true) {
            if (getCollidingObject() == paddle) {
                //Reverse the ball in both direction of it hit one of paddle side
                if (paddle.getY() - ball.getY() < BALL_RADIUS * 2 - (vy + 1)) {
                    vx = -vx;
                    vy = -vy;
                    while (paddle.getX() - ball.getX() < BALL_RADIUS * 2 - (vy + 1)) {
                        moveBall();
                        pause(20);
                    }
                } else
                    vy = -vy;
            }
            //While hit the bricks,change ball vy to opposite value, remove brick and decrease total number of it
            if (getCollidingObject() != null) {
                if (getCollidingObject() != paddle) {
                    vy = -vy;
                    remove(getCollidingObject());
                    NUMBER_OF_BRICKS--;
                }
            }
            moveBall();
            pause(20);
            //Stop the game and decrease number of life when ball hits bottom of canvas
            if (ball.getY() + BALL_RADIUS * 2 >= APPLICATION_HEIGHT - 23) {
                NTURNS--;
                break;
            }
            //Stop the game when no brick left
            if (NUMBER_OF_BRICKS < 1) {
                break;
            }
        }
    }

    /**
     * Move ball in x and y direction with speed vx and vy, and also control ball reflection
     * from canvas sides.
     */
    public void moveBall() {
        if (ball.getX() <= 0 || ball.getX() + BALL_RADIUS * 2 >= APPLICATION_WIDTH) {
            vx = -vx;
        }
        if (ball.getY() <= 0) {
            vy = -vy;
        }
        ball.setLocation(ball.getX() + vx, ball.getY() + vy);
    }

    public void mouseMoved(MouseEvent e) {
        double paddle_X;
        double last_paddle_Y = paddle.getLocation().getY();
        if (e.getX() >= APPLICATION_WIDTH - PADDLE_WIDTH) {
            paddle_X = APPLICATION_WIDTH - PADDLE_WIDTH;
        } else {
            paddle_X = e.getX();
        }
        paddle.setLocation(paddle_X, last_paddle_Y);
    }

}