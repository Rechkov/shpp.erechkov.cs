package com.shpp.erechkov.cs;

import com.shpp.karel.KarelTheRobot;

/**
 * Finding the middle
 */
public class Assignment1Part3 extends KarelTheRobot {
    public void run() throws Exception {
        move();
        while (frontIsClear()) { // Fill south line with beepers,leaving first cell and last cell empty.
            putBeeper();
            move();
        }
        goPreviousCell();
        do {
            checkNoBeepersAhead();
        } while (beepersPresent());
        goPreviousCell();
        putBeeper();

    }

    public void goPreviousCell() throws Exception {
        turnAround();
        move();
    }

    /**
     * Move forward until beepers under karel exsist.
     * Check while beepers present if no turn around and move to previous
     * cell where beepers present,and pick the beeper.
     * Decreasing number of beepers from one side.
     */
    public void checkNoBeepersAhead() throws Exception {
        while (beepersPresent()) {
            move();
        }
        goPreviousCell();
        pickBeeper();
        move();
    }

    public void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}
