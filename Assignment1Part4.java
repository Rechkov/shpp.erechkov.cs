package com.shpp.erechkov.cs;

import com.shpp.karel.KarelTheRobot;

/**
 * Chessboard
 * Created by BOSS on 30.04.2016.
 */
public class Assignment1Part4 extends KarelTheRobot {
    public void run() throws Exception {
        if (leftIsClear() && frontIsBlocked()) {//Special case for one dimensional world - 1 cell height.
            turnLeft();
        }
        while (frontIsClear() || leftIsClear()) {
            fillFromFirstCell();
            while (notFacingNorth()) { // set Karel direction strictly to north.
                turnLeft();
            }
            if (leftIsClear() && frontIsClear()) {//right is blocked by wall,means Karel on Odd Row.
                if (beepersPresent()) {//Check if last cell of previous row was filled by beeper.
                    moveFromOddRowToEvenRow();
                    move();
                } else
                    moveFromOddRowToEvenRow();
            } else if (leftIsBlocked() && frontIsClear()) {//left is blocked ,means Karel on Even Row.
                if (beepersPresent()) {//Check if last cell of previous row was filled by beeper.
                    moveFromEvenRowToOddRow();
                    move();
                } else
                    moveFromEvenRowToOddRow();
            } else {
                break;
            }
        }
    }

    public void moveFromOddRowToEvenRow() throws Exception {
        move();
        turnLeft();
    }

    public void moveFromEvenRowToOddRow() throws Exception {
        move();
        turnRight();
    }

    /**
     * Puts beepers from start position of Karel
     * until it hit the wall in chess order.
     */
    public void fillFromFirstCell() throws Exception {
        while (frontIsClear()) {
            if (noBeepersPresent()) {
                putBeeper();
            }
            if (frontIsClear()) {
                move();
                if (frontIsClear()) {
                    move();
                    if (noBeepersPresent()) {
                        putBeeper();//Put beepers in last cell of row if no obstacle.
                    }
                }
            }
        }
    }

    public void turnRight() throws Exception {
        turnLeft();
        turnLeft();
        turnLeft();
    }
}
