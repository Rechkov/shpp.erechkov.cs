package com.shpp.erechkov.cs;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.Color;


/**
 * This class create canvas 300X300 px,also add 4 black circle in the corners and
 * one white rect in the center above all others circles.
 */
public class Assignment2Part2 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;

    public void run() {
        int circle_diameter = 100;
        GOval circle = new GOval(circle_diameter, circle_diameter);
        circle.setFilled(true);
        add(circle);
        GOval circle2 = new GOval(getWidth() - circle_diameter, 0, circle_diameter, circle_diameter);
        circle2.setFilled(true);
        add(circle2);
        GOval circle3 = new GOval(0, getHeight() - circle_diameter, circle_diameter, circle_diameter);
        circle3.setFilled(true);
        add(circle3);
        GOval circle4 = new GOval(getWidth() - circle_diameter, getHeight() - circle_diameter, circle_diameter, circle_diameter);
        circle4.setFilled(true);
        add(circle4);
        GRect rect = new GRect(circle_diameter / 2, circle_diameter / 2, getWidth() - circle_diameter, getHeight() - circle_diameter);
        rect.setColor(Color.white);
        rect.setFilled(true);
        rect.setFillColor(Color.white);
        add(rect);
        System.out.println(getWidth());
    }

}
