package com.shpp.erechkov.cs;

import com.shpp.cs.a.console.TextProgram;

import java.util.Scanner;

/**
 * Created by BOSS on 05.05.2016.
 */
public class Assignment3Part2 extends TextProgram {
    public void run() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int n = Integer.parseInt(scan.nextLine());
        int temp_n;
        while (n != 1) {
            if (n % 2 == 0) {
                temp_n = n;
                n = n / 2;
                System.out.println(temp_n + " is even so I take half: " + n);
            } else {
                temp_n = n;
                n = 3 * n + 1;
                System.out.println(temp_n + " is odd so I make 3n+1: " + n);
            }
        }
    }

}
