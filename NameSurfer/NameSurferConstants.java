package com.shpp.erechkov.cs.NameSurfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

import static com.sun.java.accessibility.util.AWTEventMonitor.addComponentListener;

/**
 * Created by BOSS on 10.06.2016.
 */
public interface NameSurferConstants {
    double START_GRID_X = 5;//Start x position that show grid offset from left side of canvas
    double HORIZONTAL_LINE_INDENTATION = 20;//Horizontal line indentation from top an bottom part of canvas
    int TEXTFIELD_WIDTH = 20;
    int NUM_OF_LINE_COLUMNS = 13;//Number of vertical lines that divide graph into columns
    int DECADE = 1900;//First decade
}
