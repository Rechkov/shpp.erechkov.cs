package com.shpp.erechkov.cs.NameSurfer;


/*
 * File: NameSurferGraph.java
 * ---------------------------
 * This class represents the canvas on which the graph of
 * names is drawn. This class is responsible for updating
 * (redrawing) the graphs whenever the list of entries changes
 * or the window is resized.
 */

import acm.graphics.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;


public class NameSurferGraph extends GCanvas
        implements NameSurferConstants, ComponentListener {

    public ArrayList<NameSurferEntry> displayEntries = new ArrayList<>();
    private GLabel nameLable;

    /**
     * Creates a new NameSurferGraph object that displays the data.
     */
    public NameSurferGraph() {
        addComponentListener(this);

    }

    /**
     * Clears the list of name surfer entries stored inside this class.
     */
    public void clear() {
        displayEntries.clear();
    }

    /* Method: addEntry(entry) */

    /**
     * Adds a new NameSurferEntry to the list of entries on the display.
     * Note that this method does not actually draw the graph, but
     * simply stores the entry; the graph is drawn by calling update.
     */
    public void addEntry(NameSurferEntry entry) {
        displayEntries.add(entry);
        //Something to wright
    }

    /**
     * Updates the display image by deleting all the graphical objects
     * from the canvas and then reassembling the display according to
     * the list of entries. Your application must call update after
     * calling either clear or addEntry; update is also called whenever
     * the size of the canvas changes.
     */
    public void update() {
        removeAll();
        drawHorizontalGridLines();
        drawGraph();

    }

    public void drawHorizontalGridLines() {
        GLine topGridLine = new GLine(START_GRID_X, HORIZONTAL_LINE_INDENTATION, getWidth(), HORIZONTAL_LINE_INDENTATION);
        add(topGridLine);
        GLine bottomGridLine = new GLine(START_GRID_X, getHeight() - HORIZONTAL_LINE_INDENTATION, getWidth(), getHeight() - HORIZONTAL_LINE_INDENTATION);
        add(bottomGridLine);
    }

    public void drawLine(double x1, double y1, double x2, double y2, Color color) {
        GLine verticalLine = new GLine(x1, y1, x2, y2);
        verticalLine.setColor(color);
        add(verticalLine);

    }

    public void drawDecade(double x, double y, int decades) {
        GLabel decade = new GLabel(Integer.toString(decades));
        decade.setLocation(x, y);
        decade.setFont("London-18");
        add(decade);
    }

    public GRect drawGraphPoint(double x, double y, Color color) {
        double size = 3;
        GRect graphPoint = new GRect(x - size / 2, y - size / 2, size, size);
        graphPoint.setFillColor(color);
        graphPoint.setFilled(true);
        graphPoint.setColor(color);
        add(graphPoint);
        return graphPoint;
    }

    public void drawNameLabel(String name, double x, double y, int rank, Color color) {
        int deviation = 2;
        x += deviation;
        if (rank == 0) {
            nameLable = new GLabel(name + " *", x, y);
        } else {
            nameLable = new GLabel(name + " " + rank, x, y);

        }
        nameLable.setFont("London-9");
        nameLable.setColor(color);
        add(nameLable);
    }

    public void drawGraph() {
        double currentHeight = getHeight();
        double currentWidth = getWidth();
        for (int i = 0; i < NUM_OF_LINE_COLUMNS - 1; i++) {
            double verticalLineX = START_GRID_X + i * (currentWidth / (NUM_OF_LINE_COLUMNS - 1));
            double nextVerticalLineX = START_GRID_X + (i + 1) * (currentWidth / (NUM_OF_LINE_COLUMNS - 1));
            /*Draw vertical line of the graph grid*/
            drawLine(verticalLineX, 0, verticalLineX, currentHeight, Color.BLACK);
            drawDecade(verticalLineX, currentHeight, DECADE + i * 10);
            double graphLineStartY;
            double graphLineEndY;
            for (NameSurferEntry a : displayEntries) {
                if (i + 1 < NUM_OF_LINE_COLUMNS && a != null) {
                    graphLineStartY = (a.getRank(i + 1) * (currentHeight - 2 * HORIZONTAL_LINE_INDENTATION) / 1000) + HORIZONTAL_LINE_INDENTATION;
                    if (i + 2 < NUM_OF_LINE_COLUMNS) {
                        graphLineEndY = (a.getRank(i + 2) * (currentHeight - 2 * HORIZONTAL_LINE_INDENTATION) / 1000) + HORIZONTAL_LINE_INDENTATION;
                    } else {
                        graphLineEndY = 0;
                    }
                    if (graphLineEndY == HORIZONTAL_LINE_INDENTATION) {
                        graphLineEndY = currentHeight - HORIZONTAL_LINE_INDENTATION;
                    }
                    if (graphLineStartY == HORIZONTAL_LINE_INDENTATION) {
                        graphLineStartY = currentHeight - HORIZONTAL_LINE_INDENTATION;
                    }
                    HashMap<String, Color> graphNameColor = NameSurferDataBase.getGraphNameColor();
                    Color color = graphNameColor.get(a.getName());
                    /*Draw graph line*/
                    if (i + 2 < NUM_OF_LINE_COLUMNS) {
                        drawLine(verticalLineX, graphLineStartY, nextVerticalLineX, graphLineEndY, color);
                    }
                    drawGraphPoint(verticalLineX, graphLineStartY, color);
                    drawNameLabel(a.getName(), verticalLineX, graphLineStartY, a.getRank(i + 1), color);
                }
            }
        }
    }


    /* Implementation of the ComponentListener interface */
    public void componentHidden(ComponentEvent e) {
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentResized(ComponentEvent e) {
        update();

    }

    public void componentShown(ComponentEvent e) {
    }


}