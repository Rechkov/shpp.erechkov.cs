package com.shpp.erechkov.cs.NameSurfer;

import acm.graphics.GLabel;
import acm.graphics.GLine;
import acm.graphics.GOval;
import com.shpp.cs.a.simple.SimpleProgram;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * Created by BOSS on 10.06.2016.
 */
public class NameSurfer extends SimpleProgram implements NameSurferConstants {

    NameSurferDataBase name = new NameSurferDataBase("files/NamesData.txt");
    NameSurferGraph graphic = new NameSurferGraph();
    JButton graph = new JButton("Graph");
    JButton clear = new JButton("Clear");
    JTextField entryField = new JTextField(TEXTFIELD_WIDTH);

    public void init() {
        add(new JLabel("Name:"), NORTH);
        add(entryField, NORTH);
        add(graph, NORTH);
        add(clear, NORTH);
        add(graphic);
        graphic.update();
        entryField.setActionCommand("Enter");
        entryField.addActionListener(this);
        graph.setActionCommand("Graph");
        clear.setActionCommand("Clear");
        addActionListeners();//Its for buttons
    }

    public void actionPerformed(ActionEvent e) {
        String getCommand = e.getActionCommand();
        NameSurferEntry findName = name.findEntry(entryField.getText());
        if (getCommand.equals("Enter")) {
            graphic.addEntry(findName);
            graphic.update();
            entryField.setText("");
        } else if (getCommand.equals("Clear")) {
            graphic.clear();
            NameSurferDataBase.clearGraphNameColor();
            name.colorCount = 0;
            graphic.update();

        } else if (getCommand.equals("Graph")) {
            graphic.addEntry(findName);
            graphic.update();
            entryField.setText("");
        }
    }


}
