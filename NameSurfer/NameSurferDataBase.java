package com.shpp.erechkov.cs.NameSurfer;

import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by BOSS on 10.06.2016.
 */
public class NameSurferDataBase extends WindowProgram implements NameSurferConstants {

    private ArrayList<NameSurferEntry> fileLines = new ArrayList<>();

    public int colorCount = 0;
    private Color[] graphColor = {Color.BLUE, Color.RED, Color.MAGENTA, Color.BLACK,};
    static HashMap<String, Color> graphNameColor = new HashMap<>();//Hash map that contain Name and appropriate Color of the graph

    public NameSurferDataBase(String filePath) {
        try {
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            while (true) {
                String line = file.readLine();
                if (line == null)
                    break;
                NameSurferEntry entry = new NameSurferEntry(line);
                if (entry != null) {
                    fileLines.add(entry);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public NameSurferEntry findEntry(String name) {
        if (!name.equals("")) {
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            System.out.println(name);
            for (NameSurferEntry i : fileLines) {
                if (i.getName().equals(name)) {
                    assignNameColor(i);
                    return i;
                }
            }
        }
        return null;
    }

    public void assignNameColor(NameSurferEntry i) {
        if (!graphNameColor.containsKey(i.getName())) {
            graphNameColor.put(i.getName(), graphColor[colorCount]);
            colorCount++;
            if (colorCount > 3) {
                colorCount = 0;
            }
        }
    }

    public static HashMap<String, Color> getGraphNameColor(){
        return   graphNameColor;
    }

    public static void clearGraphNameColor(){
        graphNameColor.clear();
    }
}
