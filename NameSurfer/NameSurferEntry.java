package com.shpp.erechkov.cs.NameSurfer;
/*
 * File: NameSurferEntry.java
 * --------------------------
 * This class represents a single entry in the database.  Each
 * NameSurferEntry contains a name and a list giving the popularity
 * of that name for each decade stretching back to 1900.
 */

import java.util.ArrayList;

public class NameSurferEntry implements NameSurferConstants {

    private String[] listOfLineData;
    /* Constructor: NameSurferEntry(line) */

    /**
     * Creates a new NameSurferEntry from a data line as it appears
     * in the data file.  Each line begins with the name, which is
     * followed by integers giving the rank of that name for each
     * decade.
     */
    public NameSurferEntry(String line) {
        listOfLineData = line.split(" ");
    }

    /* Method: getName() */

    /**
     * Returns the name associated with this entry.
     */
    public String getName() {
        return listOfLineData[0];
    }

    /* Method: getRank(decade) */

    /**
     * Returns the rank associated with an entry for a particular
     * decade.  The decade value is an integer indicating how many
     * decades have passed since the first year in the database,
     * which is given by the constant START_DECADE.  If a name does
     * not appear in a decade, the rank value is 0.
     */
    public int getRank(int decade) {
        return Integer.parseInt(listOfLineData[decade]);//Where decade from 1 to 12
    }

    /* Method: toString() */

    /**
     * Returns a string that makes it easy to see the value of a
     * NameSurferEntry.
     */
    public String toString() {
        String numData = "";
        for (int i = 1; i < NUM_OF_LINE_COLUMNS; i++) {
            numData += (i + 1 < NUM_OF_LINE_COLUMNS) ? listOfLineData[i] + " " : listOfLineData[i];
        }
        return getName() + " [" + numData + "]";
    }
}
