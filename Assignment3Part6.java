package com.shpp.erechkov.cs;

import com.shpp.cs.a.graphics.WindowProgram;
import acm.graphics.*;

import java.util.Random;
import java.awt.*;

/**
 * Red  ball bounce with gravity.
 */
public class Assignment3Part6 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;
    private static final Color CARDINAL_RED = new Color(196, 30, 58);
    Random rand = new Random();

    int diameter = 40;
    int startX = 50;
    int startY = 70;
    double vx = rand.nextInt(6) - rand.nextInt(6);//starting Speed in X direction
    double vy = rand.nextInt(5) - rand.nextInt(5);//Starting speed in Y direction
    private GObject gobj;

    public void run() {
        GOval ball = new GOval(startX, startY, diameter, diameter);
        ball.setFillColor(CARDINAL_RED);
        ball.setFilled(true);
        ball.setColor(Color.BLACK);
        add(ball);
        GRect rect = new GRect(getWidth() / 2, getHeight() / 2, 50, 70);
        rect.setFillColor(Color.green);
        rect.setFilled(true);
        add(rect);
        System.out.println(getElementAt(getWidth() / 2, getHeight() / 2));
        while (true) {
            if (gobj != null) {
                remove(gobj);
            }
            if ((ball.getLocation().getX() >= getWidth() - diameter) || (ball.getLocation().getX() <= 0)) {
                vx = -vx * 0.9;
            }
            if (ball.getLocation().getY() >= getHeight() - diameter) {
                vy = -vy * 0.9;//speed loss due to collision

            }

            ball.setLocation(ball.getLocation().getX() + vx, ball.getLocation().getY() + vy);
            pause(30);
            vy = vy + 1;
            gobj = getElementAt(ball.getLocation().getX() + diameter, ball.getLocation().getY() + diameter);
        }

    }
}
