package com.shpp.erechkov.cs;

import com.shpp.karel.KarelTheRobot;

/**
 * Rows of stones
 * Created by BOSS on 29.04.2016.
 */
public class Assignment1Part2 extends KarelTheRobot {
    public void run() throws Exception {
        turnLeft();
        while (frontIsClear()) {
            fillColumnCircle();
            if (frontIsClear()) {
                moveBetweenColumns();
            }
        }
    }

    /**
     * Fill column with beepers,put only in case cell is empty.
     * Fill until hit the obstacle.
     */
    public void fillColumn() throws Exception {
        while (frontIsClear()) {
            move();
            while (noBeepersPresent()) {
                putBeeper();
            }
        }
    }

    /**
     * This method fill the column with beepers and revert to start position.
     */
    public void fillColumnCircle() throws Exception {
        fillColumn();
        turnAround();
        fillColumn();
        turnLeft();
    }

    /**
     * As we know distance between columns,this method move
     * Karel from previous start position to the next and point him to the north.     *
     */
    public void moveBetweenColumns() throws Exception {
        for (int i = 0; i < 4; i++) {
            move();
        }
        turnLeft();
    }

    public void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }


}
